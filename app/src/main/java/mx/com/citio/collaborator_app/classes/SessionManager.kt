package mx.com.citio.collaborator_app.classes

import android.content.Context
import android.content.Context.MODE_PRIVATE
import mx.com.citio.collaborator_app.api.App

object SessionManager {
    private const val MY_PREFS_NAME = "user_preferences"
    private const val SESSION_KEY = "sessionToken"

    fun saveSession(context: Context, token: String) {
        context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit().apply {
            putString(SESSION_KEY, token)
            apply()
        }
    }

    fun restoreSession(context: Context): String? {
        val editor = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE)
        return editor.getString(SESSION_KEY, null)
    }

    fun clearSession(context: Context) {
        context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit().clear().apply()
        App.authorization = null
        App.currentUser = null
    }
}