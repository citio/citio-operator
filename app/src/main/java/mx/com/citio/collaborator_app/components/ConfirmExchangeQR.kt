package mx.com.citio.collaborator_app.components

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.TextView
import mx.com.citio.collaborator_app.R

class ConfirmExchangeQR(
    context: Context,
    title: String,
    message: String
) : AlertDialog(context) {

    private lateinit var btnPositive: Button
    private lateinit var btnNegative: Button

    init {
        val view = layoutInflater.inflate(R.layout.dialog_confirm_exchange, null)
        val tvTitle: TextView = view.findViewById(R.id.tvTitle)
        val tvMessage: TextView = view.findViewById(R.id.tvMessage)
        btnPositive = view.findViewById(R.id.btnPositive)
        btnNegative = view.findViewById(R.id.btnNegative)


        tvTitle.text = title
        tvMessage.text = message

        btnPositive.visibility = View.GONE
        btnNegative.visibility = View.GONE

        this.setView(view)
        this.setCancelable(false)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window?.attributes?.windowAnimations = R.style.SlideFromDown
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    fun addAction(action: CustomDialogAction): ConfirmExchangeQR {
        when (action.forAction) {
            CustomDialogAction.ActionType.POSITIVE -> {
                btnPositive.text = action.title
                btnPositive.setOnClickListener { action.action(this) }
                btnPositive.visibility = View.VISIBLE
            }
            CustomDialogAction.ActionType.NEGATIVE -> {
                btnNegative.text = action.title
                btnNegative.setOnClickListener { action.action(this) }
                btnNegative.visibility = View.VISIBLE
            }
        }

        return this
    }

    data class CustomDialogAction(
        val title: String,
        val forAction: ActionType,
        val action: (dialog: ConfirmExchangeQR) -> Unit
    ) {
        enum class ActionType {
            POSITIVE, NEGATIVE, NEUTRAL
        }
    }


    enum class DialogIcon {
        SUCCESS, ERROR, WARNING, DANGER, NONE
    }
}