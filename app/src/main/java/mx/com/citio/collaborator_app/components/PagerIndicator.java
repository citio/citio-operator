package mx.com.citio.collaborator_app.components;

/**
 * Created by adrianmb on 13/06/18.
 */

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.widget.ViewPager2;

import java.util.ArrayList;

import mx.com.citio.collaborator_app.R;

public class PagerIndicator extends LinearLayout {

    ArrayList<ImageView> indicators = new ArrayList<>();

    public PagerIndicator(Context context) {
        super(context);
        init(null);
    }

    public PagerIndicator(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public PagerIndicator(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    public PagerIndicator(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    private void init(@Nullable AttributeSet attrs) {
    }

    public void setWithViewPager(ViewPager viewPager) {
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                for (ImageView iv : indicators) {
                    if ((int) iv.getTag() < position + 1) {
                        iv.setColorFilter(getContext().getColor(R.color.colorAccent));
                    } else {
                        iv.setColorFilter(getContext().getColor(R.color.lightGray));
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        int pages = viewPager.getAdapter().getCount();

        for (int i = 0; i < pages; i++) {
            ImageView view = new ImageView(getContext());
            view.setPadding(5, 5, 5, 5);
            view.setImageResource(R.drawable.circle_button);
            if (i == 0)
                view.setColorFilter(getContext().getColor(R.color.colorAccent));
            else
                view.setColorFilter(getContext().getColor(R.color.lightGray));
            view.setTag(i);
            this.addView(view);
            indicators.add(view);
        }

        invalidate();
        requestLayout();
    }

    public void setWithViewPager(ViewPager2 viewPager) {
        viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                for (ImageView iv : indicators) {
                    if ((int) iv.getTag() == position) {
                        iv.setColorFilter(getContext().getColor(R.color.colorAccent));
                    } else {
                        iv.setColorFilter(getContext().getColor(R.color.lightGray));
                    }
                }
            }
        });

        int pages = viewPager.getAdapter().getItemCount();

        indicators.clear();
        this.removeAllViews();

        for (int i = 0; i < pages; i++) {
            ImageView view = new ImageView(getContext());
            view.setPadding(5, 5, 5, 5);
            view.setImageResource(R.drawable.circle_button);
            if (i == 0)
                view.setColorFilter(getContext().getColor(R.color.colorAccent));
            else
                view.setColorFilter(getContext().getColor(R.color.lightGray));
            view.setTag(i);
            this.addView(view);
            indicators.add(view);
        }

        invalidate();
        requestLayout();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        this.setOrientation(HORIZONTAL);

        invalidate();
        requestLayout();
    }
}

