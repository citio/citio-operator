package mx.com.citio.collaborator_app

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.zxing.integration.android.IntentIntegrator

import kotlinx.android.synthetic.main.activity_scan_battery.*
import kotlinx.android.synthetic.main.activity_scan_battery.btnAssignMeBattery
import kotlinx.android.synthetic.main.activity_scan_battery.btnBack
import kotlinx.android.synthetic.main.activity_scan_battery.errorView
import kotlinx.android.synthetic.main.activity_scan_battery.progressBar
import kotlinx.android.synthetic.main.activity_scan_battery.recyclerView
import kotlinx.android.synthetic.main.item_error.*
import mx.com.citio.BatteryAutocompleteQuery
import mx.com.citio.collaborator_app.adapters.SubRfidAdapter
import mx.com.citio.collaborator_app.api.App
import mx.com.citio.collaborator_app.classes.onClick
import mx.com.citio.collaborator_app.components.ConfirmExchangeQR
import mx.com.citio.collaborator_app.components.CustomDialog


class ScanBatteryActivity : AppCompatActivity(), SubRfidAdapter.SubRfidAdapterListener{

    companion object {
        const val REQUEST_CODE = 25214

        fun start(context: Context, parent: Any, operatorId: String) {
            val operatorId = operatorId

            val intent = Intent(context, ScanBatteryActivity::class.java).apply {
                putExtra("operatorId", operatorId)

            }

            if (parent is Fragment)
                parent.startActivityForResult(intent, REQUEST_CODE)
            else if (parent is AppCompatActivity)
                parent.startActivityForResult(intent, REQUEST_CODE)
        }
    }

    var serialNumber :String? = ""
    lateinit var operatorId: String
    lateinit var  batterySelected: BatteryAutocompleteQuery.BatteryAutocomplete
    var batteriesScaned= ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scan_battery)
        supportActionBar?.hide()

        operatorId = intent.getStringExtra("operatorId")!!

        val layoutManager = LinearLayoutManager(this)
        layoutManager.orientation = LinearLayoutManager.VERTICAL

        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = SubRfidAdapter(this, batteriesScaned, this)


        btnScanBattery.onClick{
            initReadingQR()
        }


        0

        btnBack.onClick { onBackPressed() }
        btnAssignMeBattery.onClick{
            closeKeyboard()
            if(batteriesScaned.size>0){
                Log.i("batteriesScanedArra1", "$batteriesScaned")
                assignMeBattery()

            }else{
                showError("Debes escanear al menos una batería")
            }

        }

        println("Tap Tag window ready ...")

    }



    private fun initReadingQR() {
        runOnUiThread {
            ConfirmExchangeQR(this, "Lectura de código de barras ", "¿Deseas leer el código de barras de una batería?")
                .addAction(
                    ConfirmExchangeQR.CustomDialogAction(
                        "ACEPTAR",
                        ConfirmExchangeQR.CustomDialogAction.ActionType.POSITIVE
                    ) { dialog ->
                        initScanner()
                        dialog.dismiss()
                    })
                .addAction(
                    ConfirmExchangeQR.CustomDialogAction(
                        "CANCELAR",
                        ConfirmExchangeQR.CustomDialogAction.ActionType.NEGATIVE
                    ) { dialog ->
                        dialog.dismiss()
                    })
                .show()

        }
    }



    private fun initScanner() {

        val integrator = IntentIntegrator(this)
        integrator.setDesiredBarcodeFormats(
            IntentIntegrator.ALL_CODE_TYPES
        )
        integrator.setPrompt("Alinea el código dentro del recuadro para escanear")
        integrator.setBeepEnabled(true)
        integrator.setOrientationLocked(false)

        integrator.initiateScan()

    }




    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.i("onActivityResult1", "${requestCode}, ${resultCode}, ${data}")
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        Log.i("onActivityResult2", "${result}")

        if (result != null) {
            if (result.contents == null) {
                Log.i("CANCELADO", "ASSIGNFRAGMENT")
                Toast.makeText(this, "Cancelado", Toast.LENGTH_LONG).show()
            } else {

                showError(null)

                Log.i("SCANNER", "El valor escaneado es:  + ${result.contents} ")
                svGridBatteries.visibility = View.VISIBLE
                val sameBatterySelected =  batteriesScaned.filter{ batterySelected-> batterySelected == result.contents}
                if(sameBatterySelected.isEmpty()){
                    batteriesScaned.add(result.contents)


                    runOnUiThread {
                        updateUi()
                    }

                }else{
                    showError("La batería ya se encuentra en la lista")
                }


                /*Toast.makeText(
                    this,
                    "El valor escaneado es: " + result.contents,
                    Toast.LENGTH_LONG
                ).show()*/
                //codeComponent.visibility = View.VISIBLE
                //codeComponent.text = "El código del artículo es: "+ result.contents




            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
        super.onActivityResult(requestCode, resultCode, data)
        Log.i("RESULT", "Request code: $requestCode Result code: $resultCode")

        if (requestCode == AssignMeBatteryActivity.REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            //TODO: fetch user and update ui
        }
    }






    private fun closeKeyboard() {

        val view = this.currentFocus
        if(view !== null){
            val inputManager = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    override fun onDeleteSubRfid(subRfid: String) {
        Log.i("SUBRFIDSELECTED", subRfid)
        batteriesScaned.remove(subRfid)
        updateUi()

    }

    private fun updateUi() {
        recyclerView.adapter = SubRfidAdapter(this, batteriesScaned, this)


    }


    private fun assignMeBattery() {

        showError(null)

        Log.i("batteriesSelectedArra", "$batteriesScaned")
        Log.i("batteriesSelectedSize", "${batteriesScaned.size}")


                runOnUiThread {
                    ConfirmExchangeQR(this, "ASIGNACIÓN DE BATERÍA","¿Estás seguro de asignarte las baterías seleccionadas?")
                        .addAction(ConfirmExchangeQR.CustomDialogAction("ACEPTAR", ConfirmExchangeQR.CustomDialogAction.ActionType.POSITIVE) { dialog ->

                            progressBar.visibility = View.VISIBLE

                           App.assignMeBattery(null, operatorId, batteriesScaned) { success, error ->
                                error?.let {
                                    //If error
                                    runOnUiThread {
                                        CustomDialog(
                                            this,
                                            CustomDialog.DialogIcon.ERROR,
                                            "¡ERROR!",
                                            error
                                        )
                                            .addAction(
                                                CustomDialog.CustomDialogAction(
                                                    "Aceptar",
                                                    CustomDialog.CustomDialogAction.ActionType.POSITIVE
                                                ) { dialog ->
                                                    super.onBackPressed()
                                                    dialog.dismiss()

                                                }).show()

                                        progressBar.visibility = View.GONE
                                    }
                                }

                                if (success)  {
                                    runOnUiThread {
                                        CustomDialog(
                                            this,
                                            CustomDialog.DialogIcon.SUCCESS,
                                            "¡Listo!",
                                            "La batería se te asignó con éxito"
                                        )
                                            .addAction(
                                                CustomDialog.CustomDialogAction(
                                                    "Aceptar",
                                                    CustomDialog.CustomDialogAction.ActionType.POSITIVE
                                                ) { dialog ->
                                                    val intent = Intent(this@ScanBatteryActivity, HomeActivity::class.java)
                                                    startActivity(intent)
                                                    finish()
                                                    dialog.dismiss()

                                                }).show()

                                        progressBar.visibility = View.GONE
                                    }

                                }

                            }



                            dialog.dismiss()
                        })
                        .addAction(ConfirmExchangeQR.CustomDialogAction("CANCELAR", ConfirmExchangeQR.CustomDialogAction.ActionType.NEGATIVE) { dialog ->
                            Toast.makeText(this, "Cancelado" +
                                    "", Toast.LENGTH_SHORT).show()
                            dialog.dismiss()
                        })
                        .show()

                }


    }



    private fun showError(error: String?) {
        runOnUiThread {
            error?.let {
                tvError.text = error
                errorView.visibility = View.VISIBLE
            } ?: run {
                tvError.text = error
                errorView.visibility = View.INVISIBLE
            }
        }
    }



}