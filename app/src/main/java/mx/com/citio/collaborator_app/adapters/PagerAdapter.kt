package mx.com.citio.collaborator_app.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import java.util.*

class PagerAdapter(fm: FragmentManager?) :
    FragmentStatePagerAdapter(fm!!) {
    var fragments: ArrayList<Fragment>
    var titles: ArrayList<String>
    fun addFragment(fragment: Fragment, title: String) {
        fragments.add(fragment)
        titles.add(title)
    }

    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getCount(): Int {
        return fragments.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return titles[position]
    }

    init {
        fragments = ArrayList()
        titles = ArrayList()
    }
}
