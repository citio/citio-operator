package mx.com.citio.collaborator_app.classes

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_mock.*
import mx.com.citio.collaborator_app.R

class MockFragment : Fragment() {
    private var title: String? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v: View =
            layoutInflater.inflate(R.layout.fragment_mock, container, false)
        title = arguments!!.getString("title")
        return v
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        tvTitle.text = title
    }

    companion object {
        fun newInstance(title: String?): MockFragment {
            val fragment = MockFragment()
            val args = Bundle()
            args.putString("title", title)
            fragment.arguments = args
            return fragment
        }
    }
}