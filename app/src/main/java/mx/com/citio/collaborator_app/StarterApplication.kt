package mx.com.citio.collaborator_app

import android.app.Application
import mx.com.citio.collaborator_app.api.App
import mx.com.citio.collaborator_app.api.Network
import mx.com.citio.collaborator_app.classes.SessionManager


class StarterApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        Network.init(this)



        SessionManager.restoreSession(this)?.let {
            App.authorization = it
        }
    }


}