package mx.com.citio.collaborator_app.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import mx.com.citio.collaborator_app.R
import mx.com.citio.collaborator_app.classes.onClick

class SubRfidAdapter(val context: Context, internal val subRfidsList: List<String>, private val listener: SubRfidAdapterListener) :
    RecyclerView.Adapter<SubRfidAdapter.SubRfidViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubRfidViewHolder {
        return SubRfidViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_rfid, parent, false)
        )
    }

    override fun getItemCount(): Int = subRfidsList.size

    override fun onBindViewHolder(holder: SubRfidViewHolder, position: Int) {
        val subRfid = subRfidsList[position]
        Log.i("Adapter sibrfid", "$subRfid")
        holder.apply {

            tvRfid.text = subRfid

            btnDeleteBagde.onClick {
                listener.onDeleteSubRfid(subRfid)
            }

        }
    }


    class SubRfidViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val container: ConstraintLayout = itemView.findViewById(R.id.containerBadge)
        val tvRfid: TextView = itemView.findViewById(R.id.tv_badge_rfid)
        val btnDeleteBagde: ImageButton = itemView.findViewById(R.id.btnDeleteBadge)

    }

    interface SubRfidAdapterListener {
        fun onDeleteSubRfid(subRfid: String)
        //fun onSelectSubRfid(subRfid: String)
    }
}