package mx.com.citio.collaborator_app.classes;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

import androidx.annotation.ColorRes;
import androidx.core.graphics.drawable.DrawableCompat;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.TimeZone;

public class Utils {

    public static String moneyFormat(Double total) {

        String s = moneyFormat(Float.parseFloat(String.valueOf(total)));
        return s;
    }

    public static String moneyFormat(float total) {
        NumberFormat format = NumberFormat.getCurrencyInstance();

        DecimalFormatSymbols decimalFormatSymbols = ((DecimalFormat) format).getDecimalFormatSymbols();
        decimalFormatSymbols.setCurrencySymbol("$");
        ((DecimalFormat) format).setDecimalFormatSymbols(decimalFormatSymbols);

        String s = format.format(total);
        return s;
    }

    public static String moneyFormat(int total) {
        String s = "$" + total + " MXN";
        return s;
    }

    public static String moneyFormat(int total, boolean zeros) {
        String s = "$" + total;
        return s;
    }


    public static String dateFormat(Date date) {
        Calendar cal = getCalendarInstance();
        cal.setTime(date);

        SimpleDateFormat format = new SimpleDateFormat("E dd-MMM-yy");
        format.setTimeZone(TimeZone.getTimeZone("America/Mexico_City"));
        String s = format.format(cal.getTime()).replace(".", "");

        return s;

    }

    public static String dateTimeFormat(Date date) {
        Calendar cal = getCalendarInstance();
        cal.setTime(date);

        SimpleDateFormat format = new SimpleDateFormat("EEEE dd MMMM");
        format.setTimeZone(TimeZone.getTimeZone("America/Mexico_City"));
        String s = format.format(cal.getTime()).replace(".", "");
        s += " " + hourFormat(date);

        return s;

    }

    public static String hourFormat(Date d) {
        Calendar calendar = getCalendarInstance();
        calendar.setTime(d);

        SimpleDateFormat simpDate;
        simpDate = new SimpleDateFormat("h:mm a");
        simpDate.setTimeZone(TimeZone.getTimeZone("America/Mexico_City"));
        return simpDate.format(calendar.getTime());
    }

    public static boolean isWithinRange(Date testDate, Date startDate, Date endDate) {
        return !(testDate.before(startDate) || testDate.after(endDate));
    }

    public static void tintMenuIcon(Context context, MenuItem item, @ColorRes int color) {
        Drawable normalDrawable = item.getIcon();
        Drawable wrapDrawable = DrawableCompat.wrap(normalDrawable);
        DrawableCompat.setTint(wrapDrawable, context.getResources().getColor(color));

        item.setIcon(wrapDrawable);
    }

    public static String loadJSONFromAsset(Activity activity, String filename, boolean log) {
        String json = null;
        try {

            InputStream is = activity.getAssets().open(filename);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            json = new String(buffer, StandardCharsets.UTF_8);

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }

        if (log) {
            Log.d("JSON", json);
        }

        return json;
    }

    public static String loadJSONFromAsset(Context context, String filename, boolean log) {
        String json = null;
        try {

            InputStream is = context.getAssets().open(filename);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            json = new String(buffer, StandardCharsets.UTF_8);

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }

        if (log) {
            Log.d("JSON", json);
        }

        return json;
    }

    public static ArrayList<String> separateTopics(String string, String token) {
        ArrayList<String> list = new ArrayList<>();

        StringTokenizer tokens = new StringTokenizer(string, token);

        while (tokens.hasMoreTokens()) {
            list.add(tokens.nextToken());
        }

        Log.d("List topics", String.valueOf(list));
        return list;
    }

    public static String toTitleCase(String str) {

        if (str == null) {
            return null;
        }

        boolean space = true;
        StringBuilder builder = new StringBuilder(str);
        final int len = builder.length();

        for (int i = 0; i < len; ++i) {
            char c = builder.charAt(i);
            if (space) {
                if (!Character.isWhitespace(c)) {
                    builder.setCharAt(i, Character.toTitleCase(c));
                    space = false;
                }
            } else if (Character.isWhitespace(c)) {
                space = true;
            } else {
                builder.setCharAt(i, Character.toLowerCase(c));
            }
        }

        return builder.toString();
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }

        int totalHeight = 0;
        for (int i = 0, len = listAdapter.getCount(); i < len; i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }


    public static String getRegularExpression(List<String> list) {
        String exp = "";
        String separator = "";

        for (String s : list) {
            exp += separator + s;
            separator = "|";
        }

        Log.w("UTILS", "RegExp: " + exp);
        return exp;
    }

    public static String getMonth(int i) {
        String[] months = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};
        return months[i];
    }

    public static String getInterval(int month, int year) {
        String[] period = {
                "1/1/" + year + " - " + " 31/1/" + year,
                "1/2/" + year + " - " + " 28/2/" + year,
                "1/3/" + year + " - " + " 31/3/" + year,
                "1/4/" + year + " - " + " 30/4/" + year,
                "1/5/" + year + " - " + " 31/5/" + year,
                "1/6/" + year + " - " + " 30/6/" + year,
                "1/7/" + year + " - " + " 31/7/" + year,
                "1/8/" + year + " - " + " 31/8/" + year,
                "1/9/" + year + " - " + " 30/9/" + year,
                "1/10/" + year + " - " + " 31/10/" + year,
                "1/11/" + year + " - " + " 30/11/" + year,
                "1/12/" + year + " - " + " 31/12/" + year
        };

        return period[month];
    }

    public static String replaceSpecials(String input) {
        String original = "áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ";
        String ascii = "aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC";
        String output = input;
        for (int i = 0; i < original.length(); i++) {
            output = output.replace(original.charAt(i), ascii.charAt(i));
        }//for i
        return output;
    }

    // This snippet hides the system bars.
    public static void hideSystemUI(View decorView) {
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    public static Calendar getCalendarInstance() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("America/Mexico_City"));
        return calendar;
    }

}
