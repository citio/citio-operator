package mx.com.citio.collaborator_app.services

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.net.Uri
import android.util.Log
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import mx.com.citio.collaborator_app.HomeActivity
import mx.com.citio.collaborator_app.R
import mx.com.citio.collaborator_app.api.App

class PushNotificationService : FirebaseMessagingService() {
    companion object {
        val CHANNEL= "Citio Notificaciones"
        val TAG = "GCM SERVICE"
    }

    override fun onNewToken(token: String) {
        Log.i("TOKEN DEVICE", "TOKEN: " + token)
        App.currentUser?.let {
            App.registerDevice(token, it)
        }
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        remoteMessage.notification?.let {
            Log.d(TAG, "REMOT: " + remoteMessage)
            Log.d(TAG, "IT1: " + "${it.clickAction}")
            Log.d(TAG, "IT2: " + "${it.tag}")
            Log.d(TAG, "IT3: " + "${it.visibility}")
            Log.d(TAG, "From: " + remoteMessage.from)
            Log.d(TAG, "Notification Title: " + it.title)
            Log.d(TAG, "Notification Message: " + it.body)

            sendNotification(it.title!!, it.body!!)
        }
    }

    private fun sendNotification(title: String, message: String) {
        val intent = Intent(this, HomeActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.putExtra("notification", "received");

        val pendingIntent = PendingIntent.getActivity(
            this, 0, intent,
            PendingIntent.FLAG_ONE_SHOT
        )
        val messageNotification = if(message.contains("&#x2F;")){
            message.replace(
            "&#x2F;",
            "/"
        )}else{
            message
        }

        val defaultSoundUri: Uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val builder: NotificationCompat.Builder = NotificationCompat.Builder(this, CHANNEL)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setSmallIcon(R.drawable.ic_citio_logo)
            .setContentTitle(title)
            .setContentText(messageNotification)
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setContentIntent(pendingIntent)

        Log.d(TAG, "Launching notification")
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(0, builder.build())
    }
}