package mx.com.citio.collaborator_app

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import android.widget.AdapterView.OnItemClickListener
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_assignme_battery.recyclerView
import kotlinx.android.synthetic.main.activity_assignme_battery.*
import kotlinx.android.synthetic.main.activity_assignme_battery.btnBack
import kotlinx.android.synthetic.main.activity_assignme_battery.errorView
import kotlinx.android.synthetic.main.activity_assignme_battery.progressBar
import kotlinx.android.synthetic.main.item_error.*
import mx.com.citio.BatteryAutocompleteQuery
import mx.com.citio.collaborator_app.adapters.SubRfidAdapter
import mx.com.citio.collaborator_app.api.App
import mx.com.citio.collaborator_app.classes.onClick
import mx.com.citio.collaborator_app.components.ConfirmExchangeQR
import mx.com.citio.collaborator_app.components.CustomDialog


class AssignMeBatteryActivity : AppCompatActivity(), SubRfidAdapter.SubRfidAdapterListener{

    companion object {
        const val REQUEST_CODE = 25214

        fun start(context: Context, parent: Any, operatorId: String) {
            val operatorId = operatorId

            val intent = Intent(context, AssignMeBatteryActivity::class.java).apply {
                putExtra("operatorId", operatorId)

            }

            if (parent is Fragment)
                parent.startActivityForResult(intent, REQUEST_CODE)
            else if (parent is AppCompatActivity)
                parent.startActivityForResult(intent, REQUEST_CODE)
        }
    }

    var serialNumber :String? = ""
    lateinit var operatorId: String
    private var batteriesArray : List<BatteryAutocompleteQuery.BatteryAutocomplete>? = null
    var batteriesSelected= ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_assignme_battery)
        supportActionBar?.hide()

        operatorId = intent.getStringExtra("operatorId")!!

        val layoutManager = LinearLayoutManager(this)
        layoutManager.orientation = LinearLayoutManager.VERTICAL

        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = SubRfidAdapter(this, batteriesSelected, this)

        App.batterieAutocomplete("") { batteries, error ->
            if(batteries !== null){
                batteriesArray = batteries.batteryAutocomplete
               var batteriesSerialNumber =  batteries.batteryAutocomplete.map { battery-> battery.idHardware }

                Log.i("RFIDSAUTOCOMPL", "${ batteriesSerialNumber }")

               runOnUiThread {
                   val adapter
                           = ArrayAdapter(this,
                       android.R.layout.select_dialog_item, batteriesSerialNumber)
                   acBatteries.setAdapter(adapter)
                   acBatteries.threshold = 1
                   acBatteries.setAdapter(adapter)


               }





            }

        }




        acBatteries.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s != null) {
                    if(s.isNotEmpty()){
                        searchBattery()

                    }else{
                        //TO DO: block btn assign

                    }
                }
            }
        })




        btnBack.onClick { onBackPressed() }
        btnAssignMeBattery.onClick{
            closeKeyboard()
            if(batteriesSelected.size>0){
                Log.i("batteriesSelectedArra1", "$batteriesSelected")
                assignMeBattery()

            }else{
                showError("Debes elegir al menos una batería")
            }

        }

        println("Tap Tag window ready ...")

    }

    private fun searchBattery(){
        svGridBatteries.visibility = View.GONE
        var textSearching = acBatteries.getText().toString()
        App.batterieAutocomplete(textSearching) { batteries, error ->
            if(batteries !== null){
                batteriesArray = batteries.batteryAutocomplete
                var batteriesSerialNumber =  batteriesArray!!.map { battery-> battery.idHardware }

                Log.i("RFIDSAUTOCOMPL", "${ batteriesSerialNumber }")
                Log.i("batteriesArray1", "${ batteriesArray }")

                runOnUiThread {

                    val adapter = ArrayAdapter(this, android.R.layout.simple_selectable_list_item, batteriesSerialNumber)
                    acBatteries.setAdapter(adapter)
                    acBatteries.threshold = 1
                    acBatteries.setAdapter(adapter)
                    acBatteries.showDropDown()
                    acBatteries.setDropDownHeight( android.view.ViewGroup.LayoutParams.WRAP_CONTENT)



                    acBatteries.setOnItemClickListener(OnItemClickListener { parent, arg1, pos, id ->
                        //acBatteries.setFocusable(false)
                        acBatteries.setDropDownHeight(0);
                        acBatteries.dismissDropDown()
                        svGridBatteries.visibility = View.VISIBLE
                        showError(null)
                        val sameBatterySelected =  batteriesSelected.filter{ batterySelected-> batterySelected == acBatteries.getText().toString()}
                        if(sameBatterySelected.isEmpty()){
                            batteriesSelected.add(acBatteries.getText().toString())
                            acBatteries.setText("")

                            runOnUiThread {
                                updateUi()
                            }

                        }else{
                            showError("La batería ya se encuentra en la lista")
                        }
                        // Log.i("FOCS", "${ acBatteries.isFocused }")

                    })


                 /*   val button
                            = findViewById<Button>(R.id.btnAssignMeBattery)
                    if (button != null)
                    {
                        button ?.setOnClickListener(View.OnClickListener {
                            val enteredText = acBatteries.getText()
                            Log.i("BatterieSelected", "${enteredText}")

                            Log.i("BatterieSelected1", "${batterySelected}")

                            assignMeBattery()

                        })
                    }*/
                }





            }

        }
    }

    private fun closeKeyboard() {

        val view = this.currentFocus
        if(view !== null){
            val inputManager = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    override fun onDeleteSubRfid(subRfid: String) {
        Log.i("SUBRFIDSELECTED", subRfid)
        batteriesSelected.remove(subRfid)
        updateUi()
    }

    private fun updateUi() {
        recyclerView.adapter = SubRfidAdapter(this, batteriesSelected, this)

    }


    private fun assignMeBattery() {

        showError(null)

        Log.i("batteriesSelectedArra", "$batteriesSelected")
        Log.i("batteriesSelectedSize", "${batteriesSelected.size}")
                runOnUiThread {
                    ConfirmExchangeQR(this, "ASIGNACIÓN DE BATERÍA","¿Estás seguro de asignarte las baterías seleccionadas?")
                        .addAction(ConfirmExchangeQR.CustomDialogAction("ACEPTAR", ConfirmExchangeQR.CustomDialogAction.ActionType.POSITIVE) { dialog ->

                            progressBar.visibility = View.VISIBLE


                            App.assignMeBattery(null, operatorId, batteriesSelected) { success, error ->
                                error?.let {
                                    //If error
                                    runOnUiThread {
                                        CustomDialog(
                                            this,
                                            CustomDialog.DialogIcon.ERROR,
                                            "¡ERROR!",
                                            error
                                        )
                                            .addAction(
                                                CustomDialog.CustomDialogAction(
                                                    "Aceptar",
                                                    CustomDialog.CustomDialogAction.ActionType.POSITIVE
                                                ) { dialog ->
                                                    super.onBackPressed()
                                                    dialog.dismiss()

                                                }).show()

                                        progressBar.visibility = View.GONE
                                    }
                                }

                                if (success)  {
                                    runOnUiThread {
                                        CustomDialog(
                                            this,
                                            CustomDialog.DialogIcon.SUCCESS,
                                            "¡Listo!",
                                            "La batería se te asignó con éxito"
                                        )
                                            .addAction(
                                                CustomDialog.CustomDialogAction(
                                                    "Aceptar",
                                                    CustomDialog.CustomDialogAction.ActionType.POSITIVE
                                                ) { dialog ->
                                                    val intent = Intent(this@AssignMeBatteryActivity, HomeActivity::class.java)
                                                    startActivity(intent)
                                                    finish()
                                                    dialog.dismiss()

                                                }).show()

                                        progressBar.visibility = View.GONE
                                    }

                                }

                            }



                            dialog.dismiss()
                        })
                        .addAction(ConfirmExchangeQR.CustomDialogAction("CANCELAR", ConfirmExchangeQR.CustomDialogAction.ActionType.NEGATIVE) { dialog ->
                            Toast.makeText(this, "Cancelado" +
                                    "", Toast.LENGTH_SHORT).show()
                            dialog.dismiss()
                        })
                        .show()

                }

    }



    private fun showError(error: String?) {
        runOnUiThread {
            error?.let {
                tvError.text = error
                errorView.visibility = View.VISIBLE
            } ?: run {
                tvError.text = error
                errorView.visibility = View.INVISIBLE
            }
        }
    }



}