package mx.com.citio.collaborator_app.classes

import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log

var four = false
var six = false

class FourDigitCardFormatWatcher : TextWatcher {
    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

    override fun afterTextChanged(s: Editable) {
        Log.i("WATCHER", s.toString())
        if (s.isNotEmpty()) {
            if (s.first().toString() == "4") {
                if (s.isNotEmpty() && s.length % 5 == 0) {
                    val c = s[s.length - 1]
                    if (space == c)
                        s.delete(s.length - 1, s.length)

                }
                if (s.isNotEmpty() && s.length % 5 == 0) {
                    val c = s[s.length - 1]
                    if (Character.isDigit(c) && TextUtils.split(s.toString(), space.toString()).size <= 3)
                        s.insert(s.length - 1, space.toString())

                }
            } else if (s.first().toString() == "3") {

                if (s.isNotEmpty() && s.length % 5 == 0 && !four && !six) {
                    val c = s[s.length - 1]
                    if (space == c) {
                        s.delete(s.length - 1, s.length)
                        four = true
                    }
                }
                if (s.isNotEmpty() && s.length % 5 == 0 && !four && !six) {
                    val c = s[s.length - 1]
                    if (Character.isDigit(c) && TextUtils.split(s.toString(), space.toString()).size <= 1
                    ) {
                        four = true
                        s.insert(s.length - 1, space.toString())
                    }
                }

                if (s.isNotEmpty() && s.length % 12 == 0 && four) {
                    val c = s[s.length - 1]
                    if (space == c) {
                        s.delete(s.length - 1, s.length)
                    }
                }
                if (s.isNotEmpty() && s.length % 12 == 0 && four) {
                    val c = s[s.length - 1]
                    if (Character.isDigit(c) && TextUtils.split(
                            s.toString(),
                            space.toString()
                        ).size <= 2
                    ) {
                        s.insert(s.length - 1, space.toString())
                    }
                }

                if (s.isNotEmpty() && s.length % 18 == 0) {
                    val c = space
                    if (space == c) {
                        s.delete(s.length - 1, s.length)
                    }
                }
                if (s.isNotEmpty() && s.length % 18 == 0) {
                    val c = space
                    if (Character.isDigit(c) && TextUtils.split(
                            c.toString(),
                            space.toString()
                        ).size <= 3
                    ) {
                        s.insert(s.length - 1, space.toString())
                    }
                }

            }
        } else {
            four = false
            six = false
        }
    }

    companion object {
        private const val space = ' '
    }
}
