package mx.com.citio.collaborator_app

import android.content.Intent
import android.net.Uri
import android.nfc.NfcAdapter
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.item_error.*
import mx.com.citio.collaborator_app.api.App
import mx.com.citio.collaborator_app.classes.onClick
import mx.com.citio.type.AppTypes

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        GoogleApiAvailability.getInstance().makeGooglePlayServicesAvailable(this)

        btnLogin.onClick {
            val username = etUsername.text.toString()
            val password = etPassword.text.toString()

            if (username.isEmpty()) {
                showError(getString(R.string.enter_username))
                return@onClick
            }

            if (password.isEmpty()) {
                showError(getString(R.string.enter_password))
                return@onClick
            }

            showError(null)
            isLoading(true)
            App.login(this, username, password) { user, error ->
                user?.let {
                    Log.w("USER", it.id)

                        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
                            if (!task.isSuccessful) {
                                Log.w("GCM", "Fetching FCM registration token failed", task.exception)
                                return@OnCompleteListener
                            }
                            App.registerDevice(task.result!!, it)
                        })

                    var roles = user.roles.let { it.filter {it.app !== AppTypes.BRAIN } }

                    var roleOperator = roles.let { it.filter { it.app === AppTypes.OPERATOR} }
                    Log.i("ROLESS",  "$roles")

                    Log.i("ROLESSOPERATOR",  "$roleOperator")
                        if(roles.size>0 && roleOperator.size>0){
                            val intent = Intent(this@LoginActivity, HomeActivity
                            ::class.java)
                            intent.setAction(NfcAdapter.ACTION_NDEF_DISCOVERED)
                            startActivity(intent)

                            finish()
                            Log.i("ROLESSADMIN",  "$roles")
                        }else {


                            showError("No tienes los permisos para entrar a la app")
                        }
                        
                } ?: run {
                    Log.e("LOGIN ERROR", error ?: "Login error")
                    showError(error)
                }
                isLoading(false)
            }
        }

        btnForgotPassword.onClick{
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(App.forgotPasswordUrl))
            startActivity(intent)
        }


        if (!App.authorization.isNullOrEmpty()) fetchUser()
    }

    private fun fetchUser() {
        showError(null)
        isLoading(true)

        App.fetchUser { user, error ->
            user?.let {
                var roles = user.roles.let { it.filter {it.app !== AppTypes.BRAIN } }

                var roleOperator = roles.let { it.filter { it.app === AppTypes.ADMIN} }
                Log.i("ROLESSFETCH",  "$roles")

                Log.i("ROLESSOPERATORFETCH",  "$roleOperator")
                if(roles.size>0 && roleOperator.size>0){
                    val intent = Intent(this@LoginActivity, HomeActivity
                    ::class.java)
                    intent.setAction(NfcAdapter.ACTION_NDEF_DISCOVERED)
                    startActivity(intent)

                    finish()
                    Log.i("ROLESSADMINFETCH",  "$roles")
                }else {


                    showError("No tienes los permisos para entrar a la app")
                }
            } ?: run {
                Log.e("LOGIN ERROR", error ?: "Getting user error")
                showError(error)
            }
            isLoading(false)
        }
    }

    private fun showError(error: String?) {
        runOnUiThread {
            error?.let {
                tvError.text = error
                errorView.visibility = View.VISIBLE
            } ?: run {
                tvError.text = error
                errorView.visibility = View.INVISIBLE
            }
        }
    }

    private fun isLoading(isLoading: Boolean) {
        runOnUiThread {
            btnLogin.visibility = if (isLoading) View.GONE else View.VISIBLE
            progressBar.visibility = if (isLoading) View.VISIBLE else View.GONE
        }
    }
}
