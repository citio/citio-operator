package mx.com.citio.collaborator_app.api

import android.location.Location
import com.google.android.gms.maps.model.LatLng
import mx.com.citio.FetchUserQuery
import mx.com.citio.GetPaymentsQuery
import mx.com.citio.type.PaymentStatusType
import mx.com.citio.type.RepeatUnitTypes
import java.util.*

//User extensions
val FetchUserQuery.Me.fullname: CharSequence?
    get() = "$name $lastname"

val FetchUserQuery.Plan.nameDescription: String
    get() = when (this.repeatUnit) {
        RepeatUnitTypes.WEEK -> "${this.displayName.toUpperCase(Locale.getDefault())} (SEMANAL)"
        RepeatUnitTypes.MONTH -> "${this.displayName.toUpperCase(Locale.getDefault())} (MENSUAL)"
        RepeatUnitTypes.YEAR -> "${this.displayName.toUpperCase(Locale.getDefault())} (ANUAL)"
        RepeatUnitTypes.UNKNOWN__ -> this.displayName.toUpperCase(Locale.getDefault())
    }


//Stations extensions
fun Station.distanceFrom(point: LatLng?): String {
    if (point == null) return "- Km"

    val locationA = Location("point A")

    locationA.latitude = this.latitude
    locationA.longitude = this.longitude

    val locationB = Location("point B")

    locationB.latitude = point.latitude
    locationB.longitude = point.longitude

    val distance = locationA.distanceTo(locationB) / 1000

    return String.format("%.2f Km", distance)
}

fun Station.floatDistanceFrom(point: LatLng?): Float {
    if (point == null) return 0.0f

    val locationA = Location("point A")

    locationA.latitude = this.latitude
    locationA.longitude = this.longitude

    val locationB = Location("point B")

    locationB.latitude = point.latitude
    locationB.longitude = point.longitude

    return locationA.distanceTo(locationB) / 1000
}

val Station.isFavorite: Boolean
    get() = App.currentUser?.favoriteStations?.map { it.id }?.contains(id) ?: false

val Station.availableBatteries: Int
    get() = batteries.charged

val GetPaymentsQuery.Data1.statusDescription: String
    get() {
        return when (this.status) {
            PaymentStatusType.NEW -> "Nuevo"
            PaymentStatusType.PROCCESSED -> "Procesado"
            PaymentStatusType.WAITING_FOR_PAYMENT -> "Esperando pago"
            PaymentStatusType.WAITING_FOR_PROCESSED -> "En espera de proceso"
            PaymentStatusType.CANCELLED -> "Cancelado"
            PaymentStatusType.UNKNOWN__ -> "-"
        }
    }