package mx.com.citio.collaborator_app

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.nfc.NfcAdapter
import android.nfc.NfcManager
import android.nfc.Tag
import android.nfc.tech.IsoDep
import android.nfc.tech.NfcA
import android.nfc.tech.NfcB
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_create_subrfid.*
import kotlinx.android.synthetic.main.activity_create_subrfid.btnBack
import kotlinx.android.synthetic.main.activity_create_subrfid.btnRegisterRfid
import kotlinx.android.synthetic.main.activity_create_subrfid.progressBar
import kotlinx.android.synthetic.main.drawer.*
import kotlinx.android.synthetic.main.item_error.*
import kotlinx.android.synthetic.main.item_rfid.*
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_create_subrfid.recyclerView
import kotlinx.android.synthetic.main.activity_payment_methods.*
import mx.com.citio.GetrfidAutocompleteQuery
import mx.com.citio.collaborator_app.adapters.SubRfidAdapter
import mx.com.citio.collaborator_app.api.App
import mx.com.citio.collaborator_app.classes.onClick
import mx.com.citio.collaborator_app.components.ConfirmExchangeQR
import mx.com.citio.collaborator_app.components.CustomDialog


class CreateSubRfidActivity : AppCompatActivity(), SubRfidAdapter.SubRfidAdapterListener{

    private var targetActivity: Class<*>? = null
    private var nfcAdapter: NfcAdapter? = null
    private var intentFiltersArray: Array<IntentFilter>? = null
    private var techListsArray: Array<Array<String>>? = null
    private var pendingIntent: PendingIntent? = null

    var serialNumber :String? = ""
    private var selectedRfid: GetrfidAutocompleteQuery.RfidAutocomplete? = null
    lateinit var rfidsArray : List<GetrfidAutocompleteQuery.RfidAutocomplete>

    lateinit var  rfidSelected: GetrfidAutocompleteQuery.RfidAutocomplete
    var rfidsScan= ArrayList<String>()
    var rfidsId= ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_subrfid)
        supportActionBar?.hide()

        val layoutManager = LinearLayoutManager(this)
        layoutManager.orientation = LinearLayoutManager.VERTICAL

        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = SubRfidAdapter(this, rfidsScan, this)


        App.rfidAutocomplete("") { rfids, error ->
            if(rfids !== null){
                rfidsArray = rfids.rfidAutocomplete
                var batteriesSerialNumber =  rfids.rfidAutocomplete.map { rfid-> rfid.serialNumber }

                Log.i("RFIDSAUTOCOMPL", "${ batteriesSerialNumber }")

                runOnUiThread {
                    val adapter
                            = ArrayAdapter(this,
                        android.R.layout.select_dialog_item, batteriesSerialNumber)
                    acRfids.setAdapter(adapter)
                    acRfids.threshold = 1
                    acRfids.setAdapter(adapter)


                }

            }

        }



        acRfids.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s != null) {
                    if(s.isNotEmpty()){
                        searchBattery()

                    }else{
                        //TO DO: block btn assign

                    }
                }
            }
        })

        Log.w("SELECTEDRFID", "Filtering rfid: ${selectedRfid}")


        btnBack.onClick { onBackPressed() }

        btnRegisterRfid.onClick{

            var enteredText = acRfids.getText().toString()
            Log.i("ENTERED TEXT", enteredText)
            if(enteredText.isEmpty()){
                showError("Selecciona un RFID")
            }else{
                showError(null)
                closeKeyboard()
                assignSubRfids()
            }

        }


        println("Tap Tag window ready ...")

        val nfcManager = getSystemService(Context.NFC_SERVICE) as NfcManager
//        nfcAdapter = nfcManager.getDefaultAdapter();
        //        nfcAdapter = nfcManager.getDefaultAdapter();
        nfcAdapter = NfcAdapter.getDefaultAdapter(this)

        if (nfcManager != null) {
            println("NFC Manager ready ...")
        }

        if (nfcAdapter != null) {
            println("NFC Adapter ready ...")
        }

        pendingIntent = PendingIntent.getActivity(
            this, 0, Intent(this, javaClass).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0
        )
        intentFiltersArray = arrayOf(IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED))
        techListsArray = arrayOf(
            arrayOf(NfcA::class.java.name), arrayOf(
                NfcB::class.java.name
            ), arrayOf(IsoDep::class.java.name)
        )
    }


    override fun onBackPressed() {
        super.onBackPressed()

        val intent = Intent(this@CreateSubRfidActivity, HomeActivity::class.java)
        startActivity(intent)
        finish()

    }
    override fun onDeleteSubRfid(subRfid: String) {
        Log.i("SUBRFIDSELECTED", subRfid)
        rfidsScan.remove(subRfid)
        updateUi()
    }

    private fun updateUi() {
        recyclerView.adapter = SubRfidAdapter(this, rfidsScan, this)

    }

    private fun searchBattery(){
        svGridRfids.visibility = View.GONE
        var textSearching = acRfids.getText().toString()
        App.rfidAutocomplete(textSearching) { rfids, error ->
            if(rfids !== null){
                rfidsArray = rfids.rfidAutocomplete
                var batteriesSerialNumber =  rfidsArray!!.map { battery-> battery.serialNumber }

                Log.i("RFIDSAUTOCOMPL", "${ batteriesSerialNumber }")
                Log.i("batteriesArray1", "${ rfidsArray }")

                runOnUiThread {

                    val adapter = ArrayAdapter(this, android.R.layout.simple_selectable_list_item, batteriesSerialNumber)
                    acRfids.setAdapter(adapter)
                    acRfids.threshold = 1
                    acRfids.setAdapter(adapter)
                    acRfids.showDropDown()
                    acRfids.setDropDownHeight( android.view.ViewGroup.LayoutParams.WRAP_CONTENT)



                    acRfids.setOnItemClickListener(AdapterView.OnItemClickListener { parent, arg1, pos, id ->
                        showError(null)
                        //acRfids.setFocusable(false)
                        acRfids.setDropDownHeight(0);
                        acRfids.dismissDropDown()
                        svGridRfids.visibility = View.VISIBLE
                        instructionsNfc.visibility = View.VISIBLE
                        closeKeyboard()
                        // Log.i("FOCS", "${ acBatteries.isFocused }")

                    })


                }
                
            }

        }
    }



    private fun assignSubRfids() {
        if(rfidsScan.isEmpty()){
            showError("Escanea tarjetas Citio para agregar SubRFIDS")

        }else{
            showError(null)
            var enteredText = acRfids.getText().toString()

            App.rfidAutocomplete(enteredText){ rfids, error ->
                if(rfids !== null && rfidsScan !== null && rfidsScan.size>0 && enteredText !== null){
                    Log.i("rfidsScan ", "$rfidsScan ")
                    Log.i("enteredText", "$enteredText")
                    rfidSelected =  rfids.rfidAutocomplete.filter { rfid-> rfid.serialNumber == enteredText.toString() }[0]
                    Log.i("selectedrfid", "$rfidSelected")

                    runOnUiThread {
                        ConfirmExchangeQR(this, "ASIGNAR SUBRFID","¿Deseas asignar subRfids a  ${rfidSelected.serialNumber}?")
                            .addAction(ConfirmExchangeQR.CustomDialogAction("ACEPTAR", ConfirmExchangeQR.CustomDialogAction.ActionType.POSITIVE) { dialog ->

                                progressBar.visibility = View.VISIBLE


                                 App.assignSubRfids(rfidSelected.id, rfidsScan, ) { success, error ->
                                      error?.let {
                                          //If error
                                          runOnUiThread {
                                              CustomDialog(
                                                  this,
                                                  CustomDialog.DialogIcon.ERROR,
                                                  "¡ERROR!",
                                                  error
                                              )
                                                  .addAction(
                                                      CustomDialog.CustomDialogAction(
                                                          "Aceptar",
                                                          CustomDialog.CustomDialogAction.ActionType.POSITIVE
                                                      ) { dialog ->
                                                         // super.onBackPressed()
                                                          dialog.dismiss()

                                                      }).show()

                                              progressBar.visibility = View.GONE
                                          }
                                      }

                                      if (success)  {
                                          runOnUiThread {
                                              CustomDialog(
                                                  this,
                                                  CustomDialog.DialogIcon.SUCCESS,
                                                  "¡Listo!",
                                                  "Los sub RFIDs fueron asignados"
                                              )
                                                  .addAction(
                                                      CustomDialog.CustomDialogAction(
                                                          "Aceptar",
                                                          CustomDialog.CustomDialogAction.ActionType.POSITIVE
                                                      ) { dialog ->
                                                          val intent = Intent(this@CreateSubRfidActivity, HomeActivity::class.java)
                                                          startActivity(intent)
                                                          finish()
                                                          dialog.dismiss()

                                                      }).show()

                                              progressBar.visibility = View.GONE
                                          }

                                      }

                                  }



                                dialog.dismiss()
                            })
                            .addAction(ConfirmExchangeQR.CustomDialogAction("CANCELAR", ConfirmExchangeQR.CustomDialogAction.ActionType.NEGATIVE) { dialog ->
                                Toast.makeText(this, "Cancelado" +
                                        "", Toast.LENGTH_SHORT).show()
                                dialog.dismiss()
                            })
                            .show()

                    }
                }


            }




        }





    }





    override fun onResume() {
        super.onResume()
        val intent = Intent()
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
        val nfcPendingIntent = PendingIntent.getActivity(this, 0, intent, 0)
        if (nfcAdapter != null) {
            nfcAdapter!!.enableForegroundDispatch(
                this,
                pendingIntent,
                intentFiltersArray,
                techListsArray
            )
        }
    }

    override fun onPause() {
        super.onPause()
        if (nfcAdapter != null) {
            try {
                nfcAdapter!!.disableForegroundDispatch(this)
            } catch (ex: IllegalStateException) {
                Log.e("ATHTAG", "Error disabling NFC foreground dispatch", ex)
            }
        }
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        println("Doing onNewIntent() ...")
        closeKeyboard()
          val SELECT = byteArrayOf(
              0x00.toByte(),  // CLA Class
              0xA4.toByte(),  // INS Instruction
              0x04.toByte(),  // P1  Parameter 1
              0x00.toByte(),  // P2  Parameter 2
              0x0A.toByte(),  // Length
              0x63, 0x64, 0x63, 0x00, 0x00, 0x00, 0x00, 0x32, 0x32, 0x31 // AID
          )

        val GET_STRING = byteArrayOf(
            0x80.toByte(), // CLA Class
            0x04, // INS Instruction
            0x00, // P1  Parameter 1
            0x00, // P2  Parameter 2
            0x10  // LE  maximal number of bytes expected in result
        )

        // Use NFC to read tag
        val tag: Tag? = intent?.getParcelableExtra(NfcAdapter.EXTRA_TAG)
        //val nfc = NfcB.get(tag)
        //val isoTag = IsoDep.get(tag)
        //Log.e("tag", "$tag")

        if (tag != null) {
            Log.e("tagID", "${tag.id}")
            Log.e("BYTESTOHEX1", "${bytesToHex(tag.id)}")
             serialNumber = bytesToHex(tag.id)
            Log.e("BYTESTOHEXserialNumber", "${serialNumber}")
            //tvEmpty.text = serialNumber
                val string = String(tag.id, Charsets.UTF_16)
            val string1 = String(tag.id, Charsets.UTF_8)

            Log.e("string", "${string}")
            Log.e("string1", "${string1}")

            if(serialNumber !== null){
               // val rfidBadgeView = View.inflate(this, R.layout.item_rfid, null) as FrameLayout
               // val tvRfid = rfidBadgeView.findViewById<TextView>(R.id.tv_badge_rfid)


              val sameRfidScan =  rfidsScan.filter{ rfidScan-> rfidScan == serialNumber}
                Log.e("FILTERSAMERFID", "${sameRfidScan}")

                if(sameRfidScan.isEmpty()){
                    instructionsNfc.visibility = View.GONE
                    showError(null)
                    rfidsScan.add(serialNumber!!)
                    //tvRfid.text = serialNumber

                    runOnUiThread {
                        updateUi()
                        //llRfids.addView(rfidBadgeView)
                    }
                }else{
                    showError("El RFID ya se encuentra agregado")
                }






               /* rlEmptyRfid.visibility = View.GONE
                llRfidData.visibility = View.VISIBLE
                valueSerialNumber.text = serialNumber*/


            }else{
                runOnUiThread {
                    CustomDialog(
                        this,
                        CustomDialog.DialogIcon.ERROR,
                        "¡ERROR!",
                        "Error al detectar RFID, intenta de nuevo"
                    )
                        .addAction(
                            CustomDialog.CustomDialogAction(
                                "Aceptar",
                                CustomDialog.CustomDialogAction.ActionType.POSITIVE
                            ) { dialog ->
                                dialog.dismiss()
                            }).show()
                }
            }

        }else{
            runOnUiThread {
                Toast.makeText(this, "No se detectó un RFID en la tarjeta", Toast.LENGTH_SHORT).show()
            }

        }

       // Log.i("tagNFCA", "${NfcA.get(tag)}")
        //Log.i("tagISO", "${isoTag}")
       // Log.i("ISOHIla", "${isoTag.hiLayerResponse}")

        }

    private val hexArray = "0123456789ABCDEF".toCharArray()
    fun bytesToHex(bytes: ByteArray?): String? {
        if (bytes == null) return null
        val hexChars = CharArray(bytes.size * 2)
        for (j in bytes.indices) {

            val v: Int = bytes.get(j).toInt() and 0xFF
            hexChars[j * 2] = hexArray.get(v ushr 4)
            hexChars[j * 2 + 1] = hexArray.get(v and 0x0F)
        }
        return String(hexChars)
    }

    private fun closeKeyboard() {

        val view = this.currentFocus
        if(view !== null){
            val inputManager = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    private fun showError(error: String?) {
        runOnUiThread {
            error?.let {
                tvError.text = error
                errorView.visibility = View.VISIBLE
            } ?: run {
                tvError.text = error
                errorView.visibility = View.INVISIBLE
            }
        }
    }


    fun setTargetRedirectIntent(activity: Class<*>) {
        targetActivity = activity
    }
}