package mx.com.citio.collaborator_app

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.nfc.NfcAdapter
import android.nfc.NfcManager
import android.nfc.Tag
import android.nfc.tech.IsoDep
import android.nfc.tech.NfcA
import android.nfc.tech.NfcB
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.HapticFeedbackConstants
import android.view.View
import android.widget.AdapterView
import android.widget.FrameLayout
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.UpdateAvailability
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.drawer.*
import mx.com.citio.collaborator_app.adapters.NavAdapter
import mx.com.citio.collaborator_app.api.App
import mx.com.citio.collaborator_app.api.fullname
import mx.com.citio.collaborator_app.classes.Navigate
import mx.com.citio.collaborator_app.classes.onClick
import mx.com.citio.collaborator_app.components.ConfirmExchangeQR
import mx.com.citio.collaborator_app.components.CustomDialog
import mx.com.citio.collaborator_app.sections.AlarmsFragment
import mx.com.citio.collaborator_app.sections.NotificationsFragment
import mx.com.citio.collaborator_app.sections.profile.ProfileFragment
import mx.com.citio.collaborator_app.sections.profile.RecordFragment
import mx.com.citio.collaborator_app.sections.stations.AssignBatteryFragment
import mx.com.citio.collaborator_app.sections.stations.InventoryFragment
import mx.com.citio.collaborator_app.services.PushNotificationService

class HomeActivity : AppCompatActivity(), HomeInteractor {
    data class NavSection(val name: String, val fragment: Fragment?, val activity: Class<out HomeActivity>?)
    //data class NavSection(val name: String, val fragment: Fragment)

    private lateinit var toggle: ActionBarDrawerToggle
    private var doubleBackToExitPressedOnce = false
    private val navList = ArrayList<NavSection>()

    private var targetActivity: Class<*>? = null
    private var nfcAdapter: NfcAdapter? = null
    private var intentFiltersArray: Array<IntentFilter>? = null
    private var techListsArray: Array<Array<String>>? = null
    private var pendingIntent: PendingIntent? = null
    private var notificationReceived: String? = null

    val MY_REQUEST_CODE = 999
    val RESULT_OK = -1
    val RESULT_CANCELED = -1


    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setSupportActionBar(toolbar)

        FirebaseMessaging.getInstance().token.addOnCompleteListener {
            it.result?.let { token ->
                Log.w("GCM", "Fetching FCM registration token $token")
            } ?: run {
                Log.e("GCM", "FCM Token not found")
            }
        }
        checkUpdate()
        createNotificationChannel()
        intent.hasExtra("notification")
        Log.e("notificationReceived1", "$intent")
        Log.e("notificationReceived2", "${intent.extras}")


        if(intent.hasExtra("notification")){
            notificationReceived = intent.getStringExtra("notification")!!
            Log.e("notificationReceived3", "$notificationReceived")

            if(notificationReceived !== null){
                val notificationFragment = NotificationsFragment.newInstance()
                Navigate.changeFragment(this, notificationFragment, Navigate.AnimationStyle.FADE_OUT)

            }
        }

        supportActionBar?.apply {
            setHomeButtonEnabled(true)
            setDisplayHomeAsUpEnabled(false)
            setDisplayShowTitleEnabled(false)
        }

        supportFragmentManager.addOnBackStackChangedListener {
            val backCount = supportFragmentManager.backStackEntryCount
            if (backCount > 0)
                supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            else
                supportActionBar!!.setDisplayHomeAsUpEnabled(false)
            toggle.syncState()
        }

        toggle = ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)

        toggle.drawerArrowDrawable.color = getColor(R.color.paleGray)

        drawer.setDrawerListener(toggle)
        toggle.syncState()

        val profileFragment = ProfileFragment.newInstance()

        navList.run {
            add(NavSection(getString(R.string.nav_inscriptions),null, this@HomeActivity::class.java))
            add(NavSection(getString(R.string.nav_rfid),null, this@HomeActivity::class.java))
            add(NavSection(getString(R.string.nav_subrfid),null, this@HomeActivity::class.java))
           // add(NavSection(getString(R.string.nav_inventory), InventoryFragment.newInstance(),null))
            App.currentUser?.let {
                add(NavSection(getString(R.string.nav_assign_battery), AssignBatteryFragment.newInstance(),null))
                //add(NavSection(getString(R.string.nav_funds), FundsFragment.newInstance(getString(R.string.nav_funds)), null))
                add(NavSection(getString(R.string.nav_notifications), NotificationsFragment.newInstance(), null))
                add(NavSection(getString(R.string.nav_alarms), AlarmsFragment.newInstance(), null))
            }
        }

        listView.adapter = NavAdapter(this, R.layout.item_nav, navList)

        listView.setOnItemClickListener { parent: AdapterView<*>?, view: View, position: Int, id: Long ->
            view.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
            ibProfile.visibility = View.VISIBLE

            drawer.closeDrawer(GravityCompat.START)
            Log.w("DRAWER", "Navigating to - " + navList.get(position).name)
            navList[position].fragment?.let {
                Navigate.changeFragment(this,
                    it, Navigate.AnimationStyle.FADE_OUT)
            }
            navList[position].activity?.let {
                val isRfid = navList.get(position).name === getString(R.string.nav_rfid)
                val isSubRfid = navList.get(position).name === getString(R.string.nav_subrfid)

                when {
                    isRfid -> {
                        val intent = Intent(this@HomeActivity, CreateRfidActivity::class.java)
                        startActivity(intent)
                        //finish()
                    }
                    isSubRfid -> {
                        val intent = Intent(this@HomeActivity, CreateSubRfidActivity::class.java)
                        startActivity(intent)
                    }
                    else -> {
                        val intent = Intent(this@HomeActivity, this::class.java)
                        startActivity(intent)
                        finish()
                    }
                }

            }
        }

        val openProfile: (view: View) -> Unit = {
            it.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)

            App.currentUser?.let {
                drawer.closeDrawer(GravityCompat.START)
                Log.w("DRAWER", "Navigating to - " + profileFragment)
                Navigate.changeFragment(this, profileFragment, Navigate.AnimationStyle.FADE_OUT)

                ibProfile.visibility = View.GONE
            } ?: run {
                Intent(this, LoginActivity::class.java).run { startActivity(this) }
                finish()
            }
        }

        rlProfile.onClick(openProfile)
        ibProfile.onClick(openProfile)
        ivLogo.onClick { navigateStationList() }

       // navigateStationList()

        App.currentUser?.let { user ->
            tvUsername.text = user.fullname
            tvLogin.text = getString(R.string.my_profile)
            tvLogin.visibility = View.GONE
        } ?: run {
            ibProfile.visibility = View.GONE
            tvLogin.visibility = View.GONE
        }


 println("Tap Tag window ready ...")

        val nfcManager = getSystemService(Context.NFC_SERVICE) as NfcManager
//        nfcAdapter = nfcManager.getDefaultAdapter();
        //        nfcAdapter = nfcManager.getDefaultAdapter();
        nfcAdapter = NfcAdapter.getDefaultAdapter(this)

        if (nfcManager != null) {
            println("NFC Manager ready ...")
        }

        if (nfcAdapter != null) {
            println("NFC Adapter ready ...")
        }

        pendingIntent = PendingIntent.getActivity(
            this, 0, Intent(this, javaClass).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0
        )
        intentFiltersArray = arrayOf(IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED))
        techListsArray = arrayOf(
            arrayOf(NfcA::class.java.name), arrayOf(
                NfcB::class.java.name
            ), arrayOf(IsoDep::class.java.name)
        )


    }

    private fun navigateStationList() {
        listView.run {
            val view = adapter.getView(0, null, FrameLayout(this@HomeActivity))
            val itemId = adapter.getItemId(0)

            performItemClick(view, 0, itemId)
        }
    }


    override fun onBackPressed() {
        invalidateOptionsMenu()
        val fm = supportFragmentManager
        if (fm.backStackEntryCount > 0) {
            fm.popBackStack()
        } else {
            if (doubleBackToExitPressedOnce) {
                finish()
                return
            }
            this.doubleBackToExitPressedOnce = true
            Toast.makeText(this, getString(R.string.press_back_exit), Toast.LENGTH_SHORT).show()
            Handler()
                .postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
        }
    }

    fun onOpenRecordFragment(type: String, rfid: String?) {
        App.currentUser?.let { ibProfile.visibility = View.VISIBLE }
        Navigate.changeFragment(
            this,
            RecordFragment.newInstance(type, rfid),
            Navigate.AnimationStyle.SLIDE_IN_LEFT,
            type
        )
    }

    override fun onOpenFundsFragment() {
        App.currentUser?.let { ibProfile.visibility = View.VISIBLE }
        navList[1].fragment?.let {
            Navigate.changeFragment(this,
                it, Navigate.AnimationStyle.FADE_OUT)
        }
    }

    fun onTransactionSuccess() {
        //TODO: Fetch user
    }

    private fun checkUpdate() {
        val appUpdateManager = AppUpdateManagerFactory.create(this)
        // Returns an intent object that you use to check for an update.
        val appUpdateInfoTask = appUpdateManager?.appUpdateInfo
        // Checks that the platform will allow the specified type of update.
        Log.d("TAG", "Checking for updates")
        appUpdateInfoTask?.addOnSuccessListener { appUpdateInfo ->
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {
                // Request the update.
                appUpdateManager.startUpdateFlowForResult(
                    // Pass the intent that is returned by 'getAppUpdateInfo()'.
                    appUpdateInfo,
                    // Or 'AppUpdateType.FLEXIBLE' for flexible updates.
                    AppUpdateType.IMMEDIATE,
                    // The current activity making the update request.
                    this,
                    // Include a request code to later monitor this update request.
                    MY_REQUEST_CODE)
                Log.d("TAG", "Update available")
            } else {
                Log.d("TAG", "No Update available")
            }
        }
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                PushNotificationService.CHANNEL,
                "Citio notifications",
                NotificationManager.IMPORTANCE_DEFAULT
            )

            channel.description = "Citio notificaciones"

            val notificationManager = getSystemService(NotificationManager::class.java)
            notificationManager.createNotificationChannel(channel)
        }
    }

    // Checks that the update is not stalled during 'onResume()'.
// However, you should execute this check at all entry points into the app.
    override fun onResume() {
        val appUpdateManager = AppUpdateManagerFactory.create(this)
        super.onResume()

        appUpdateManager.appUpdateInfo
            .addOnSuccessListener { appUpdateInfo ->

                if (appUpdateInfo.updateAvailability()
                    == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS
                ) {
                    // If an in-app update is already running, resume the update.
                    appUpdateManager.startUpdateFlowForResult(
                        appUpdateInfo,
                        AppUpdateType.IMMEDIATE,
                        this,
                        MY_REQUEST_CODE
                    );
                }
            }


            val intent = Intent()
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
        val nfcPendingIntent = PendingIntent.getActivity(this, 0, intent, 0)
        if (nfcAdapter != null) {
            nfcAdapter!!.enableForegroundDispatch(
                this,
                pendingIntent,
                intentFiltersArray,
                techListsArray
            )
        }

        
    }

    override fun onPause() {
        super.onPause()
        if (nfcAdapter != null) {
            try {
                nfcAdapter!!.disableForegroundDispatch(this)
            } catch (ex: IllegalStateException) {
                Log.e("ATHTAG", "Error disabling NFC foreground dispatch", ex)
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
         super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == MY_REQUEST_CODE && resultCode !== RESULT_OK) {
           Toast.makeText(this, "Cancelado",Toast.LENGTH_SHORT).show()
        }
    }




     override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        println("Doing onNewIntent() ...")

        val tag: Tag? = intent?.getParcelableExtra(NfcAdapter.EXTRA_TAG)
        //Log.e("tag", "$tag")

        if (tag != null) {
            Log.e("tagID", "${tag.id}")
            Log.e("BYTESTOHEX1", "${bytesToHex(tag.id)}")
            val serialNumber = bytesToHex(tag.id)
            Log.e("BYTESTOHEXserialNumber", "${serialNumber}")

            if(serialNumber !== null){
                runOnUiThread {
                    ConfirmExchangeQR(this, getString(R.string.register_inscription),"¿Deseas registrar el RFID ${serialNumber}?")
                        .addAction(ConfirmExchangeQR.CustomDialogAction(getString(R.string.register), ConfirmExchangeQR.CustomDialogAction.ActionType.POSITIVE) { dialog ->
                            progress.visibility = View.VISIBLE

                            App.createRfidInscription(serialNumber) { rfid, error ->
                                error?.let {
                                    //If error
                                    runOnUiThread {
                                        CustomDialog(
                                            this,
                                            CustomDialog.DialogIcon.ERROR,
                                            "¡ERROR!",
                                            error
                                        )
                                            .addAction(
                                                CustomDialog.CustomDialogAction(
                                                    "Aceptar",
                                                    CustomDialog.CustomDialogAction.ActionType.POSITIVE
                                                ) { dialog ->
                                                    dialog.dismiss()

                                                }).show()

                                        progress.visibility = View.GONE
                                    }
                                }

                                rfid?.let {
                                    runOnUiThread {
                                        CustomDialog(
                                            this,
                                            CustomDialog.DialogIcon.SUCCESS,
                                            "¡Listo!",
                                            "La inscripción ha sido creada con éxito"
                                        )
                                            .addAction(
                                                CustomDialog.CustomDialogAction(
                                                    getString(R.string.label_continue),
                                                    CustomDialog.CustomDialogAction.ActionType.POSITIVE
                                                ) { dialog ->

                                                    rlEmptyInscription.visibility = View.GONE
                                                    llRfidData.visibility = View.VISIBLE
                                                    barcodeRfid.text = rfid.inscription?.barcode
                                                    serialNumberRfid.text = serialNumber
                                                    folioRfid.text = rfid.inscription?.folio
                                                    dialog.dismiss()
                                                    //finish()
                                                }).show()

                                        progress.visibility = View.GONE
                                    }

                                }

                            }


                            dialog.dismiss()
                        })
                        .addAction(ConfirmExchangeQR.CustomDialogAction("CANCELAR", ConfirmExchangeQR.CustomDialogAction.ActionType.NEGATIVE) { dialog ->
                            Toast.makeText(this, "Cancelado" +
                                    "", Toast.LENGTH_SHORT).show()
                            dialog.dismiss()
                        })
                        .show()

                }
            }else{
                runOnUiThread {
                    CustomDialog(
                        this,
                        CustomDialog.DialogIcon.ERROR,
                        "¡ERROR!",
                        "Error al detectar RFID, intenta de nuevo"
                    )
                        .addAction(
                            CustomDialog.CustomDialogAction(
                                "Aceptar",
                                CustomDialog.CustomDialogAction.ActionType.POSITIVE
                            ) { dialog ->
                                dialog.dismiss()
                            }).show()
                }
            }


        }else{
            runOnUiThread {
                Toast.makeText(this, "No se detectó un RFID en la tarjeta", Toast.LENGTH_SHORT).show()
            }

        }


        }

        private val hexArray = "0123456789ABCDEF".toCharArray()
        fun bytesToHex(bytes: ByteArray?): String? {
        if (bytes == null) return null
        val hexChars = CharArray(bytes.size * 2)
        for (j in bytes.indices) {

            val v: Int = bytes.get(j).toInt() and 0xFF
            hexChars[j * 2] = hexArray.get(v ushr 4)
            hexChars[j * 2 + 1] = hexArray.get(v and 0x0F)
        }
        return String(hexChars)
    }


    fun setTargetRedirectIntent(activity: Class<*>) {
        targetActivity = activity
    }
}

interface HomeInteractor {
    //fun onOpenRecordFragment(type: String, rfid: String?)

    fun onOpenFundsFragment()
    //fun onTransactionSuccess()
}