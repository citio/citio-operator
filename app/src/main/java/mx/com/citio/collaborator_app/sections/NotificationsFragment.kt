package mx.com.citio.collaborator_app.sections

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_notifications.*
import mx.com.citio.collaborator_app.R
import mx.com.citio.collaborator_app.adapters.NotificationAdapter
import mx.com.citio.collaborator_app.api.App
import mx.com.citio.collaborator_app.api.CitioNotification

class NotificationsFragment : Fragment() {

    var notifications: ArrayList<CitioNotification> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return layoutInflater.inflate(R.layout.fragment_notifications, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        listView.adapter = NotificationAdapter(context!!, R.layout.item_notification, notifications) {
            App.readNotification(it) { success, error ->
                activity?.runOnUiThread { loadNotifications() }
            }
        }

        loadNotifications()
    }

    private fun loadNotifications() {
        progressBar.visibility = View.VISIBLE
        tvEmpty.visibility = View.GONE
        listView.visibility = View.GONE

        this.notifications.clear()
        App.loadNotifications { notifications ->
            activity?.runOnUiThread {
                Log.e("NOTIFICATIONS", "$notifications")
                this.notifications.addAll(notifications)

                (listView.adapter as NotificationAdapter).notifyDataSetChanged()

                progressBar.visibility = View.GONE

                if (this.notifications.isEmpty()) {
                    tvEmpty.visibility = View.VISIBLE
                    listView.visibility = View.GONE
                } else {
                    tvEmpty.visibility = View.GONE
                    listView.visibility = View.VISIBLE
                }
            }
        }
    }

    companion object {
        fun newInstance(): NotificationsFragment = NotificationsFragment()
    }
}