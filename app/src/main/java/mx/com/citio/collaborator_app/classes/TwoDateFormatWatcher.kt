package mx.com.citio.collaborator_app.classes

import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log

class TwoDateFormatWatcher : TextWatcher {
    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

    override fun afterTextChanged(s: Editable) {
        Log.wtf("cero", s.toString())

        if (s.isNotEmpty() && (s.first().toString() == "2" || s.first()
                .toString() == "3" || s.first().toString() == "4" || s.first()
                .toString() == "5" || s.first().toString() == "6" || s.first()
                .toString() == "7" || s.first().toString() == "8" || s.first().toString() == "9")
        ) {
            val c = s[s.length - 1]
            if (cero == c) {
                s.delete(s.length - 1, s.length)
            }
        }

        if (s.isNotEmpty() && (s.first().toString() == "2" || s.first()
                .toString() == "3" || s.first().toString() == "4" || s.first()
                .toString() == "5" || s.first().toString() == "6" || s.first()
                .toString() == "7" || s.first().toString() == "8" || s.first().toString() == "9")
        ) {
            val c = s[s.length - 1]
            if (Character.isDigit(c) && TextUtils.split(
                    s.toString(),
                    cero.toString()
                ).size <= 1
            ) {
                s.insert(s.length - 1, cero.toString())
            }
        }

        if (s.isNotEmpty() && s.length % 3 == 0) {
            val c = s[s.length - 1]
            if (space == c) {
                s.delete(s.length - 1, s.length)
            }
        }
        if (s.isNotEmpty() && s.length % 3 == 0) {
            val c = s[s.length - 1]
            if (Character.isDigit(c) && TextUtils.split(
                    s.toString(),
                    space.toString()
                ).size <= 1
            ) {
                s.insert(s.length - 1, space.toString())
            }
        }
    }

    companion object {
        private const val space = '/'
        private const val cero = '0'
    }
}
