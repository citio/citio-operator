package mx.com.citio.collaborator_app.adapters

import android.content.Context
import android.graphics.Color
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageButton
import android.widget.TextView
import mx.com.citio.collaborator_app.R
import mx.com.citio.collaborator_app.api.CitioAlarm
import mx.com.citio.collaborator_app.api.CitioNotification
import mx.com.citio.collaborator_app.classes.format
import mx.com.citio.collaborator_app.classes.onClick
import mx.com.citio.collaborator_app.classes.toDate


class AlarmAdapter(context: Context, val layout: Int, val list: ArrayList<CitioAlarm>, val onReadNotification: (notification: CitioAlarm) -> Unit) :
    ArrayAdapter<CitioAlarm>(context, layout, list) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val v = View.inflate(context, layout, null)

        val alarm = list[position]

        val tvDate = v.findViewById<TextView>(R.id.tvDate)
        val tvTitleAlarm = v.findViewById<TextView>(R.id.tvTitleNotification)
        val tvMessage = v.findViewById<TextView>(R.id.tvMessage)
        val ibDelete = v.findViewById<ImageButton>(R.id.ibDelete)

        tvMessage.text =if (alarm.description.contains("&#x2F;")) {
            alarm.description.replace(
                "&#x2F;",
                "/"
            )
        } else alarm.description
        tvDate.text = alarm.createdAt.toLong().toDate().format("d / MMMM / yyyy")

       /* v.setBackgroundColor(
            if (alarm.timesRead == 0) Color.parseColor("#E5ECDD") else Color.parseColor("#FBFBFB")
        )*/

        ibDelete.onClick { onReadNotification(alarm) }

        return v
    }
}