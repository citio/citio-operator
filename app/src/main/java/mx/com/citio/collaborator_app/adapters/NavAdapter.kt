package mx.com.citio.collaborator_app.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import mx.com.citio.collaborator_app.HomeActivity
import mx.com.citio.collaborator_app.R

class NavAdapter(context: Context, val layout: Int, val list: ArrayList<HomeActivity.NavSection>) :
    ArrayAdapter<HomeActivity.NavSection>(context, layout, list) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = View.inflate(context, layout, null)

        val tvSection = view.findViewById<TextView>(R.id.tvSection)
        tvSection.text = list[position].name

        return view
    }
}