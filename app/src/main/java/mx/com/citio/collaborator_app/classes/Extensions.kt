package mx.com.citio.collaborator_app.classes

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.TypedValue
import android.view.View
import android.widget.EditText
import android.widget.TextView
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

fun View.onClick(clickListener: (view: View) -> Unit) {
    this.setOnClickListener(clickListener)
}

fun Date.format(pattern: String = "MM/dd/yyyy"): String = SimpleDateFormat(pattern).format(this)

fun Long.toDate(): Date = Date(this)

val Double.moneyFormat: String
    get() = DecimalFormat("$#,###,###,##0.00").format(this)

fun EditText.bindWith(textView: TextView, callback: ((value: String) -> Unit)? = null) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {}
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            textView.text = s.toString()
            callback?.let { it(s.toString()) }
        }
    })
}

fun Float.dp(context: Context): Int {
    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, this, context.resources.displayMetrics).toInt()
}