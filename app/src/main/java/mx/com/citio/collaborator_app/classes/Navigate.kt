package mx.com.citio.collaborator_app.classes

import android.util.Log
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import mx.com.citio.collaborator_app.R

object Navigate {
    fun changeFragment(
        activity: AppCompatActivity,
        fragment: Fragment,
        animationStyle: AnimationStyle, @Nullable tag: String? = null
    ) {
        val fragmentManager: FragmentManager = activity.supportFragmentManager
        val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
        setAnimation(fragmentTransaction, animationStyle, tag == null)
        if (tag != null) fragmentTransaction.addToBackStack(tag)
        fragmentTransaction.replace(R.id.content, fragment)
        fragmentTransaction.commit()
        if (tag == null) {
            try {
                fragmentManager.popBackStackImmediate(
                    null,
                    FragmentManager.POP_BACK_STACK_INCLUSIVE
                )
            } catch (e: Exception) {
                Log.e("STACK", e.message ?: "error changing fragment")
            }
        }
    }



  /*  fun changeActivity(
        originActivity: AppCompatActivity,
        targetActivity: AppCompatActivity,
        animationStyle: AnimationStyle, @Nullable tag: String? = null
    ){
        val intent = Intent(originActivity, targetActivity::class.java)
        startActivity(intent)
        finish()
    }*/

    private fun setAnimation(
        transaction: FragmentTransaction,
        animationStyle: AnimationStyle,
        singleAnimation: Boolean
    ) {
        if (singleAnimation) when (animationStyle) {
            AnimationStyle.SLIDE_IN_RIGHT -> transaction.setCustomAnimations(
                R.anim.slide_in_right,
                R.anim.slide_out_left,
                R.anim.slide_in_left,
                R.anim.slide_out_right
            )
            AnimationStyle.SLIDE_IN_LEFT -> transaction.setCustomAnimations(
                R.anim.slide_in_right,
                R.anim.slide_out_left,
                R.anim.slide_in_left,
                R.anim.slide_out_right
            )
            AnimationStyle.SLIDE_IN_UP -> transaction.setCustomAnimations(
                R.anim.slide_in_right,
                R.anim.slide_out_left,
                R.anim.slide_in_left,
                R.anim.slide_out_right
            )
            AnimationStyle.FADE_OUT -> transaction.setCustomAnimations(
                R.anim.fadein,
                R.anim.fadeout,
                R.anim.fadein,
                R.anim.fadeout
            )
        } else when (animationStyle) {
            AnimationStyle.SLIDE_IN_RIGHT -> transaction.setCustomAnimations(
                R.anim.slide_in_right,
                R.anim.slide_out_left
            )
            AnimationStyle.SLIDE_IN_LEFT -> transaction.setCustomAnimations(
                R.anim.slide_in_right,
                R.anim.slide_out_left,
                R.anim.slide_in_left,
                R.anim.slide_out_right
            )
            AnimationStyle.SLIDE_IN_UP -> transaction.setCustomAnimations(
                R.anim.slide_in_right,
                R.anim.slide_out_left,
                R.anim.slide_in_left,
                R.anim.slide_out_right
            )
            AnimationStyle.FADE_OUT -> transaction.setCustomAnimations(
                R.anim.fadein,
                R.anim.fadeout
            )
        }
    }

    enum class AnimationStyle {
        SLIDE_IN_RIGHT, SLIDE_IN_LEFT, SLIDE_IN_UP, FADE_OUT
    }
}
