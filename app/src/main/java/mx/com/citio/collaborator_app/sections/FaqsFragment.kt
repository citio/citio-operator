package mx.com.citio.collaborator_app.sections

import android.os.Bundle
import android.view.HapticFeedbackConstants
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_faqs.*
import mx.com.citio.collaborator_app.R
import mx.com.citio.collaborator_app.api.App
import mx.com.citio.collaborator_app.api.Faq

class FaqsFragment : Fragment() {

    private lateinit var faqs: List<Faq>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v: View =
            layoutInflater.inflate(R.layout.fragment_faqs, container, false)
        return v
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)

        App.loadFaqs {
            it.forEachIndexed { index, faq ->
                val faqView = View.inflate(context, R.layout.item_faq, null) as LinearLayout

                val tvQuestion = faqView.findViewById<TextView>(R.id.tvQuestion)
                val tvAnswer = faqView.findViewById<TextView>(R.id.tvAnswer)
                val rlQuestion = faqView.findViewById<RelativeLayout>(R.id.rlQuestion)
                val ivArrow = faqView.findViewById<ImageView>(R.id.ivArrow)

                tvQuestion.text = faq.question
                tvAnswer.text = faq.answer
                tvAnswer.visibility = View.GONE

                rlQuestion.setOnClickListener { v ->
                    v.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
                    if (tvAnswer.visibility == View.GONE) {
                        tvAnswer.visibility = View.VISIBLE
                        ivArrow.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp)
                    } else {
                        tvAnswer.visibility = View.GONE
                        ivArrow.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp)
                    }
                }

                if (index == 0){
                    rlQuestion.performClick()
                    ivArrow.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp)
                } else {
                    tvAnswer.visibility = View.GONE
                    ivArrow.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp)
                }

                activity?.runOnUiThread {
                    llFaqs.addView(faqView)
                }
            }
        }
    }

    companion object {
        fun newInstance(): FaqsFragment {
            val fragment = FaqsFragment()

            fragment.arguments = Bundle().apply {

            }

            return fragment
        }
    }
}