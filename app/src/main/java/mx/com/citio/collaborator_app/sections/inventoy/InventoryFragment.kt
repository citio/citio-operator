package mx.com.citio.collaborator_app.sections.stations

import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.zxing.integration.android.IntentIntegrator
import kotlinx.android.synthetic.main.fragment_inventory.*
import mx.com.citio.FetchUserQuery
import mx.com.citio.collaborator_app.R
import mx.com.citio.collaborator_app.api.App
import mx.com.citio.collaborator_app.api.Station
import mx.com.citio.collaborator_app.classes.onClick
import mx.com.citio.collaborator_app.components.ConfirmExchangeQR

class InventoryFragment : Fragment() {

    private val REQUEST_PERMISSION_CODE = 100
    private var selectedRfid: FetchUserQuery.Rfid? = null
    private lateinit var map: GoogleMap
    private var currentLocation: LatLng? = null

    private var stations: ArrayList<Station> = ArrayList()
    private var markers: ArrayList<Marker> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return layoutInflater.inflate(R.layout.fragment_inventory, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        App.currentUser?.let { user ->

            user.rfids.firstOrNull().let {
                selectedRfid = it
            }
        }
        btnScanner.onClick { initReadingQR() }
    }


    private fun isLoading(isLoading: Boolean) {
        activity?.runOnUiThread {
            progressBar.visibility = if (isLoading) View.VISIBLE else View.GONE
        }
    }

    private fun showError(error: String?) {
        if (error !== null) {
            ConfirmExchangeQR(context!!, "ERROR AL REALIZAR EL CAMBIO", error)
                .addAction(
                    ConfirmExchangeQR.CustomDialogAction(
                        "Ok",
                        ConfirmExchangeQR.CustomDialogAction.ActionType.POSITIVE
                    ) { dialog ->
                        dialog.dismiss()
                    }).show()
        }
    }

    private fun initReadingQR() {
        activity?.runOnUiThread {
            ConfirmExchangeQR(context!!, "Escanear componente", "¿Deseas escanear un componente?")
                .addAction(
                    ConfirmExchangeQR.CustomDialogAction(
                        "REALIZAR ESCANEO",
                        ConfirmExchangeQR.CustomDialogAction.ActionType.POSITIVE
                    ) { dialog ->
                        initScanner()
                        dialog.dismiss()
                    })
                .addAction(
                    ConfirmExchangeQR.CustomDialogAction(
                        "CANCELAR",
                        ConfirmExchangeQR.CustomDialogAction.ActionType.NEGATIVE
                    ) { dialog ->
                        dialog.dismiss()
                    })
                .show()

        }
    }

    private fun initScanner() {

        val integrator = IntentIntegrator.forSupportFragment(this)
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES
        )
        integrator.setPrompt("Alinea el código dentro del recuadro para escanear")
        integrator.setBeepEnabled(true)
        integrator.setOrientationLocked(false)

        integrator.initiateScan()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.i("onActivityResult1", "${requestCode}, ${resultCode}, ${data}")
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        Log.i("onActivityResult2", "${result}")

        if (result != null) {
            if (result.contents == null) {
                Log.i("CANCELADO", "STATIONFRAGMEN")
                Toast.makeText(activity, "Cancelado", Toast.LENGTH_LONG).show()
            } else {
               // if (result.contents.contains("stationId")) {
                    showError(null)
                    //Log.i("SCANNER", "El rfid es:  ${selectedRfid?.id}")
                    /*val stationId = JSONObject(result.contents).get("stationId").toString()
                    val qrCodeId = JSONObject(result.contents).get("qrOrderId").toString()*/
                    Log.i("SCANNER", "El valor escaneado es:  + ${result.contents} ")
                    Toast.makeText(
                        activity,
                        "El valor escaneado es: " + result.contents,
                        Toast.LENGTH_LONG
                    ).show()
                    codeComponent.visibility = View.VISIBLE
                    codeComponent.text = "El código del artículo es: "+ result.contents
                    /* selectedRfid?.let {
                        isLoading(true)
                        App.executeQrOrderData(it.id, stationId, qrCodeId ) { success, error ->
                            Log.i("responseEXECUTEQR", "RESPONSE:  $success, $error")

                            error?.let {
                                var errorExecute = ""
                                when (error) {
                                    "The qrOrder doesn't exists" -> errorExecute = "El qrOder no existe"
                                    else -> errorExecute = error
                                }
                                activity?.runOnUiThread {
                                    showError(errorExecute)
                                }
                            }
                            if(success){
                                activity?.runOnUiThread {
                                    ConfirmExchangeQR(context!!, "Intercambio finalizado","Hemos ejecutado correctamente tu intercambio, verifica la estación para finalizarlo correctamente")
                                        .addAction(ConfirmExchangeQR.CustomDialogAction("CONTINUAR", ConfirmExchangeQR.CustomDialogAction.ActionType.POSITIVE) { dialog ->
                                            dialog.dismiss()
                                        }).show()
                                }

                            }
                        }
                        isLoading(false)
                    }*/
              /*  } else {
                    activity?.runOnUiThread {
                        showError("El código QR no es válido")
                    }

                }*/
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }


    companion object {
        fun newInstance(): InventoryFragment = InventoryFragment()
    }
}
