package mx.com.citio.collaborator_app.components

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import mx.com.citio.collaborator_app.R

class CustomDialog(
    context: Context,
    icon: DialogIcon = DialogIcon.NONE,
    title: String,
    message: String
) : AlertDialog(context) {

    private lateinit var btnPositive: Button

    init {
        val view = layoutInflater.inflate(R.layout.dialog_custom, null)

        val ivIcon: ImageView = view.findViewById(R.id.ivIcon)
        val tvTitle: TextView = view.findViewById(R.id.tvTitle)
        val tvMessage: TextView = view.findViewById(R.id.tvMessage)
        btnPositive = view.findViewById(R.id.btnPositive)

        when (icon) {
            DialogIcon.SUCCESS -> {
                ivIcon.setImageDrawable(context.getDrawable(R.drawable.ic_tick))
                tvTitle.backgroundTintList  =AppCompatResources.getColorStateList(context, R.color.lightGreen)
            }
            DialogIcon.ERROR -> {
                tvTitle.backgroundTintList  =AppCompatResources.getColorStateList(context, R.color.lightRed)
                ivIcon.setImageDrawable(context.getDrawable(R.drawable.ic_exclamationmark))
            }
            DialogIcon.WARNING -> ivIcon.setImageDrawable(context.getDrawable(R.drawable.ic_warning))
            DialogIcon.DANGER -> TODO()
            DialogIcon.NONE -> ivIcon.visibility = View.GONE
        }

        tvTitle.text = title
        tvMessage.text = message


        btnPositive.visibility = View.GONE

        this.setView(view)
        this.setCancelable(false)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window?.attributes?.windowAnimations = R.style.SlideFromDown
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    fun addAction(action: CustomDialogAction): CustomDialog {
        when (action.forAction) {
            CustomDialogAction.ActionType.POSITIVE -> {
                btnPositive.text = action.title
                btnPositive.setOnClickListener { action.action(this) }
                btnPositive.visibility = View.VISIBLE
            }

        }

        return this
    }

    data class CustomDialogAction(
        val title: String,
        val forAction: ActionType,
        val action: (dialog: CustomDialog) -> Unit
    ) {
        enum class ActionType {
            POSITIVE, NEGATIVE, NEUTRAL
        }
    }


    enum class DialogIcon {
        SUCCESS, ERROR, WARNING, DANGER, NONE
    }
}