package mx.com.citio.collaborator_app.sections

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_help.*
import mx.com.citio.collaborator_app.R
import mx.com.citio.collaborator_app.SupportActivity
import mx.com.citio.collaborator_app.TutorialActivity
import mx.com.citio.collaborator_app.api.App

class HelpFragment : Fragment() {
    private var title: String? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v: View =
            layoutInflater.inflate(R.layout.fragment_help, container, false)
        title = arguments!!.getString("title")
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        btnCall.text = App.Contact.phone
        btnEmail.text = App.Contact.email

        btnWhatsapp.setOnClickListener {
            val url = "whatsapp://send?phone=${App.Contact.whatsapp}"
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            try {
                startActivity(i)
            } catch (exception: Exception) {
                "Instala la aplicación whatsapp".toast(context!!)
            }
        }

        btnCall.setOnClickListener {
            val dialIntent = Intent()
            dialIntent.action = Intent.ACTION_DIAL
            dialIntent.data = Uri.parse("tel:${App.Contact.phone}")
            startActivity(dialIntent)
        }

        btnEmail.setOnClickListener {
            val emailIntent =
                Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", App.Contact.email, null))
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Contacto desde Citio Android")
            startActivity(Intent.createChooser(emailIntent, "Enviar correo"))
        }

        btnTicket.setOnClickListener {
            val intent = Intent(activity, SupportActivity::class.java)
            startActivity(intent)
        }

        btnTutorial.setOnClickListener {
            val intent = Intent(activity, TutorialActivity::class.java)
            startActivity(intent)
        }
    }

    companion object {
        fun newInstance(): HelpFragment {
            val fragment = HelpFragment()

            fragment.arguments = Bundle().apply {

            }

            return fragment
        }
    }
}

private fun String.toast(context: Context) {
    Toast.makeText(context, this, Toast.LENGTH_LONG).show()
}
