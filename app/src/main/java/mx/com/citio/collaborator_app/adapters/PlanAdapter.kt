package mx.com.citio.collaborator_app.adapters

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import mx.com.citio.collaborator_app.R
import mx.com.citio.collaborator_app.api.Plan
import mx.com.citio.collaborator_app.classes.moneyFormat
import mx.com.citio.type.RepeatUnitTypes

class PlanAdapter(val context: Context, val plans: List<Plan>) :
    RecyclerView.Adapter<PlanAdapter.PlanViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlanViewHolder {
        return PlanViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_plan, parent, false)
        )
    }

    override fun getItemCount(): Int = plans.size

    override fun onBindViewHolder(holder: PlanViewHolder, position: Int) {
        val plan = plans[position]

        holder.apply {
            val colorValue = java.lang.Long.parseLong(plan.color.replace("#", ""), 16)
            if (colorValue < 3487029) tvName.setTextColor(context.getColor(android.R.color.white)) else tvName.setTextColor(context.getColor(android.R.color.black))

            llContainer.backgroundTintList = ColorStateList.valueOf(Color.parseColor(plan.color))
            tvName.text = plan.displayName
            tvPrice.text = plan.price.moneyFormat
            tvExchanges.text = if (plan.unlimited === true) "∞"  else plan.exchanges.toString()
            tvKilometeres.text = if (plan.unlimited === true) "∞" else "${plan.kms}Km"
            tvExtraExchange.text = if (plan.priceExtraExchange == 0.0) "Sin costo" else plan.priceExtraExchange.moneyFormat

            tvRepetition.text = when (plan.repeatUnit) {
                RepeatUnitTypes.WEEK -> "semanalidad (${plan.currency})"
                RepeatUnitTypes.MONTH -> "mensualidad (${plan.currency})"
                RepeatUnitTypes.YEAR -> "anualidad (${plan.currency})"
                RepeatUnitTypes.UNKNOWN__ -> "(${plan.currency})"
            }
        }
    }


    class PlanViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val llContainer: LinearLayout = itemView.findViewById(R.id.llContainer)
        val tvName: TextView = itemView.findViewById(R.id.tvName)
        val tvPrice: TextView = itemView.findViewById(R.id.tvPrice)
        val tvRepetition: TextView = itemView.findViewById(R.id.tvRepetition)
        val tvExchanges: TextView = itemView.findViewById(R.id.tvExchanges)
        val tvKilometeres: TextView = itemView.findViewById(R.id.tvKilometers)
        val tvExtraExchange: TextView = itemView.findViewById(R.id.tvExtraExchange)
    }
}