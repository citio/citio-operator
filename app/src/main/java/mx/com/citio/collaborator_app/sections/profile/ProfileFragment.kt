package mx.com.citio.collaborator_app.sections.profile

import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_profile.*
import mx.com.citio.FetchUserQuery
import mx.com.citio.collaborator_app.HomeInteractor
import mx.com.citio.collaborator_app.LoginActivity
import mx.com.citio.collaborator_app.R
import mx.com.citio.collaborator_app.api.App
import mx.com.citio.collaborator_app.api.fullname
import mx.com.citio.collaborator_app.api.nameDescription
import mx.com.citio.collaborator_app.classes.SessionManager
import mx.com.citio.collaborator_app.classes.format
import mx.com.citio.collaborator_app.classes.onClick
import mx.com.citio.collaborator_app.classes.toDate
import mx.com.citio.type.ContractStatuses

class ProfileFragment : Fragment() {
    companion object {
        fun newInstance(): ProfileFragment = ProfileFragment().apply {
            arguments = Bundle().apply {}
        }
    }

    private lateinit var homeInteractor: HomeInteractor
    private var selectedRfid: FetchUserQuery.Rfid? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return layoutInflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        App.currentUser?.let { user ->
            tvUsername.text = user.fullname

            user.rfids.firstOrNull().let {
                this.selectedRfid = it
            }

           // updateUi()
        }


        var plan = selectedRfid?.inscription?.contract?.plan?.displayName
        var status = selectedRfid?.status?.status
        /*when (plan) {
            "Libre" -> {
                btnExchange.text="Cambiar método de pago"
            }
        }
        when(status){
            ContractStatuses.BLOCKED -> btnExchange.text="Cambiar método de pago"
            ContractStatuses.CANCEL -> btnExchange.text="Cambiar método de pago"
            ContractStatuses.CLOSE -> btnExchange.text="Cambiar método de pago"
            ContractStatuses.EXPIRED -> btnExchange.text="Cambiar método de pago"
            ContractStatuses.WAITING_FOR_DELIVERY_BATTERY -> btnExchange.text="Cambiar método de pago"
        }

        btnPlans.onClick {
            homeInteractor.onOpenFundsFragment()
        }

        btnExchange.onClick {
            homeInteractor.onOpenFundsFragment()
        }

        rlExchangesRecord.onClick {
            homeInteractor.onOpenRecordFragment("orders", selectedRfid?.id)
        }

        rlPaymentsRecord.onClick {
            homeInteractor.onOpenRecordFragment("payments", selectedRfid?.id)
        }*/

        btnLogout.onClick {
            SessionManager.clearSession(context!!)
            activity?.finish()
            Intent(context, LoginActivity::class.java).run { startActivity(this) }
        }
    }



    enum class StatusType {
        OK, INFO, WARNING
    }


    override fun onResume() {
        super.onResume()
      //  updateUi()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        homeInteractor =
            if (context is HomeInteractor) context else throw IllegalStateException("$context must implement HomeInteractor interface")
    }
}