package mx.com.citio.collaborator_app.sections.profile

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.button_back.*
import kotlinx.android.synthetic.main.fragment_record.*
import mx.com.citio.collaborator_app.R
import mx.com.citio.collaborator_app.adapters.RecordAdapter
import mx.com.citio.collaborator_app.api.App
import mx.com.citio.collaborator_app.api.OrdersPage
import mx.com.citio.collaborator_app.api.PaymentsPage
import mx.com.citio.collaborator_app.api.statusDescription
import mx.com.citio.collaborator_app.classes.onClick

class RecordFragment : Fragment() {
    companion object {
        fun newInstance(type: String, rfid: String?): RecordFragment {
            val fragment = RecordFragment()
            fragment.arguments = Bundle().apply {
                putString("rfid", rfid)
                putString("type", type)
            }
            return fragment
        }
    }

    var rfid: String? = null
    lateinit var type: RecordType

    val limit = 20
    private var paymentsPage: PaymentsPage? = null
    private var ordersPage: OrdersPage? = null

    var records: ArrayList<RecordAdapter.Record> = ArrayList()

    private val currentPage: Int
        get() = when (type) {
            RecordType.PAYMENT -> paymentsPage?.myPayments?.params?.page ?: 0
            RecordType.ORDER -> ordersPage?.myOrders?.params?.page ?: 0
        }

    private val totalPages: Int
        get() = when (type) {
            RecordType.PAYMENT -> paymentsPage?.myPayments?.params?.totalPages ?: 0
            RecordType.ORDER -> ordersPage?.myOrders?.params?.totalPages ?: 0
        }


    enum class RecordType {
        PAYMENT, ORDER
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return layoutInflater.inflate(R.layout.fragment_record, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rfid = arguments?.getString("rfid")
        arguments?.getString("type", "payments")?.let {
            type = if (it == "payments") RecordType.PAYMENT else RecordType.ORDER
        }

        listView.adapter = RecordAdapter(context!!, R.layout.item_record, records)

        tvRecordTitle.text = when (type) {
            RecordType.PAYMENT -> "Mi historial de pagos"
            RecordType.ORDER -> "Mi historial de recargas"
        }

        btnFirst.visibility = View.INVISIBLE
        btnPrevious.visibility = View.INVISIBLE
        btnNext.visibility = View.INVISIBLE
        btnLast.visibility = View.INVISIBLE

        btnFirst.onClick {
            loadPage(0)
        }

        btnPrevious.onClick {
            loadPage(currentPage - 1)
        }

        btnNext.onClick {
            loadPage(currentPage + 1)
        }

        btnLast.onClick {
            loadPage(totalPages - 1)
        }

        btnBack.onClick {
            activity?.onBackPressed()
        }

        rfid?.let {
            Log.i("RECORDS", "Loading records page")
            loadPage(currentPage)
        } ?: run {
            Log.w("RECORDS", "RFID not found")
            progress.visibility = View.GONE
            tvEmpty.visibility = View.VISIBLE
            listView.visibility = View.GONE
        }
    }

    private fun loadPage(pageNum: Int) {
        progress.visibility = View.VISIBLE
        tvEmpty.visibility = View.GONE
        listView.visibility = View.GONE

        records.clear()

        when (type) {
            RecordType.PAYMENT -> {
                App.loadPaymentsPage(rfid!!, pageNum, limit) { page, error ->
                    page?.let { paymentPage ->
                        paymentsPage = paymentPage
                        records.addAll(paymentPage.myPayments.data.map {
                            RecordAdapter.Record(it.createdAt, it.amount, it.card ?: it.statusDescription)
                        })
                        activity?.runOnUiThread {
                            (listView.adapter as RecordAdapter).notifyDataSetChanged()

                            progress.visibility = View.GONE
                            if (records.isEmpty()) {
                                tvEmpty.visibility = View.VISIBLE
                                listView.visibility = View.GONE
                            } else {
                                tvEmpty.visibility = View.GONE
                                listView.visibility = View.VISIBLE
                            }
                            updateControls()
                        }
                    }

                    error?.let {

                    }
                }
            }
            RecordType.ORDER -> {
                App.loadOrdersPage(rfid!!, pageNum, limit) { page, error ->
                    page?.let { orderPage ->
                        ordersPage = orderPage
                        records.addAll(orderPage.myOrders.data.map {
                            RecordAdapter.Record(it.createdAt, 0.00, it.station.shortName)
                        })
                        activity?.runOnUiThread {
                            (listView.adapter as RecordAdapter).notifyDataSetChanged()

                            progress.visibility = View.GONE
                            if (records.isEmpty()) {
                                tvEmpty.visibility = View.VISIBLE
                                listView.visibility = View.GONE
                            } else {
                                tvEmpty.visibility = View.GONE
                                listView.visibility = View.VISIBLE
                            }
                            updateControls()
                        }
                    }

                    error?.let {

                    }
                }
            }
        }
    }

    private fun updateControls() {
        if (totalPages <= 1) {
            //hide all
            btnFirst.visibility = View.INVISIBLE
            btnPrevious.visibility = View.INVISIBLE
            btnNext.visibility = View.INVISIBLE
            btnLast.visibility = View.INVISIBLE
            return
        }

        if (currentPage == 0) {
            //hide first and previous
            btnFirst.visibility = View.INVISIBLE
            btnPrevious.visibility = View.INVISIBLE
            btnNext.visibility = View.VISIBLE
            btnLast.visibility = View.VISIBLE
            return
        }

        if (currentPage == totalPages - 1) {
            //hide next and last
            btnFirst.visibility = View.VISIBLE
            btnPrevious.visibility = View.VISIBLE
            btnNext.visibility = View.INVISIBLE
            btnLast.visibility = View.INVISIBLE
            return
        }

        if (currentPage != 0 && currentPage != totalPages - 1) {
            //show all
            btnFirst.visibility = View.VISIBLE
            btnPrevious.visibility = View.VISIBLE
            btnNext.visibility = View.VISIBLE
            btnLast.visibility = View.VISIBLE
            return
        }
    }
}