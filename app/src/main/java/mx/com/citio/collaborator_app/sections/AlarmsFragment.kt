package mx.com.citio.collaborator_app.sections

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_alarms.*
import kotlinx.android.synthetic.main.fragment_notifications.listView
import kotlinx.android.synthetic.main.fragment_notifications.tvEmpty
import mx.com.citio.GetAlarmsQuery
import mx.com.citio.collaborator_app.R
import mx.com.citio.collaborator_app.adapters.AlarmAdapter
import mx.com.citio.collaborator_app.api.AlarmsPage
import mx.com.citio.collaborator_app.api.App
import mx.com.citio.collaborator_app.api.CitioAlarm
import mx.com.citio.collaborator_app.classes.onClick

class AlarmsFragment : Fragment() {

    private var alarmsPage: AlarmsPage? = null

    var alarms: ArrayList<CitioAlarm> = ArrayList()
    val limit = 20
    private val currentPage: Int
        get() = alarmsPage?.alarms?.params?.page ?: 0

    private val totalPages: Int
        get() = alarmsPage?.alarms?.params?.totalPages ?: 0


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return layoutInflater.inflate(R.layout.fragment_alarms, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        listView.adapter = AlarmAdapter(context!!, R.layout.item_notification, alarms) {
           loadAlarms(currentPage)

        }

        btnFirst.visibility = View.INVISIBLE
        btnPrevious.visibility = View.INVISIBLE
        btnNext.visibility = View.INVISIBLE
        btnLast.visibility = View.INVISIBLE

        btnFirst.onClick {
            loadAlarms(0)
        }

        btnPrevious.onClick {
            loadAlarms(currentPage - 1)
        }

        btnNext.onClick {
            loadAlarms(currentPage + 1)
        }

        btnLast.onClick {
            loadAlarms(totalPages - 1)
        }
        loadAlarms(currentPage)
    }

    private fun loadAlarms(pageNum: Int) {
        progress.visibility = View.VISIBLE
        tvEmpty.visibility = View.GONE
        listView.visibility = View.GONE

        this.alarms.clear()
        App.loadAlarms(pageNum, limit) { page ->
            page?.let { alarmPage ->
                alarmsPage = alarmPage

                activity?.runOnUiThread {
                    this.alarms.addAll(alarmPage.alarms.data)
                    Log.e("ALARMS", "$alarms")

                    (listView.adapter as AlarmAdapter).notifyDataSetChanged()

                    progress.visibility = View.GONE

                    if (this.alarms.isEmpty()) {
                        tvEmpty.visibility = View.VISIBLE
                        listView.visibility = View.GONE
                    } else {

                        tvEmpty.visibility = View.GONE
                        listView.visibility = View.VISIBLE
                    }
                    updateControls()
                }

            }

        }
    }


    private fun updateControls() {
        Log.d("curr","Total${totalPages}, Curr ${currentPage ==0}")
        Log.d("curr1","Total${totalPages <= 1}, Curr ${currentPage ==0}")
        Log.d("curr12","Total${currentPage == totalPages - 1}, ")
        Log.d("curr123","Total${currentPage != 0 && currentPage != totalPages - 1}, ")

        if (totalPages <= 1) {
            //hide all
            btnFirst.visibility = View.INVISIBLE
            btnPrevious.visibility = View.INVISIBLE
            btnNext.visibility = View.INVISIBLE
            btnLast.visibility = View.INVISIBLE
            return
        }

        if (currentPage == 0) {
            Log.d("curr"," ${currentPage} yes")

            //hide first and previous
            btnFirst.visibility = View.INVISIBLE
            btnPrevious.visibility = View.INVISIBLE
            btnNext.visibility = View.VISIBLE
            btnLast.visibility = View.VISIBLE
            return
        }

        if (currentPage == totalPages - 1) {
            //hide next and last
            btnFirst.visibility = View.VISIBLE
            btnPrevious.visibility = View.VISIBLE
            btnNext.visibility = View.INVISIBLE
            btnLast.visibility = View.INVISIBLE
            return
        }

        if (currentPage != 0 && currentPage != totalPages - 1) {
            //show all
            btnFirst.visibility = View.VISIBLE
            btnPrevious.visibility = View.VISIBLE
            btnNext.visibility = View.VISIBLE
            btnLast.visibility = View.VISIBLE
            return
        }
    }
    companion object {
        fun newInstance(): AlarmsFragment = AlarmsFragment()
    }
}