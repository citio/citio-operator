package mx.com.citio.collaborator_app.adapters

import android.content.Context
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter
import com.google.android.gms.maps.model.Marker
import mx.com.citio.collaborator_app.R
import mx.com.citio.collaborator_app.api.Station
import mx.com.citio.collaborator_app.api.availableBatteries

class SnippetAdapter(val context: Context) : InfoWindowAdapter {
    override fun getInfoWindow(marker: Marker): View? {
        val view: View =
            (context as AppCompatActivity).layoutInflater.inflate(R.layout.snippet_station, null)
        val station: Station = marker.tag as Station

        view.findViewById<TextView>(R.id.tvStationName).run { text = station.shortName }
        if(!station.comingSoon){
            view.findViewById<TextView>(R.id.tvAvailableRecharges).run { text = "DISPONIBLES:" }
            view.findViewById<ImageView>(R.id.ivAvailable).visibility = View.VISIBLE
            view.findViewById<ImageView>(R.id.ivAvailable).run {
                if (station.availableBatteries > 0)
                    setImageResource(R.drawable.ic_tick)
                else
                    setImageResource(R.drawable.ic_close)
            }
        }else{
            view.findViewById<TextView>(R.id.tvAvailableRecharges).run { text = "PROXIMAMENTE" }
            view.findViewById<ImageView>(R.id.ivAvailable).visibility = View.INVISIBLE
        }


        return view
    }

    override fun getInfoContents(marker: Marker): View? {
        return null
    }
}
