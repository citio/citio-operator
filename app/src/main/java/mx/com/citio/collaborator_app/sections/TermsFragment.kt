package mx.com.citio.collaborator_app.sections

import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_terms.*
import mx.com.citio.collaborator_app.R
import mx.com.citio.collaborator_app.api.App

class TermsFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        layoutInflater.inflate(R.layout.fragment_terms, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        App.getTerms { terms ->
            activity?.runOnUiThread {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    tvTerms.text = Html.fromHtml(terms, Html.FROM_HTML_MODE_COMPACT)
                } else {
                    tvTerms.text = Html.fromHtml(terms)
                }
            }
        }
    }

    companion object {
        fun newInstance(): TermsFragment = TermsFragment()
    }
}