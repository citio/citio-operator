package mx.com.citio.collaborator_app

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_support.*
import kotlinx.android.synthetic.main.button_back.*
import mx.com.citio.collaborator_app.R
import mx.com.citio.collaborator_app.api.App
import mx.com.citio.collaborator_app.classes.onClick
import mx.com.citio.collaborator_app.components.CustomDialog

class SupportActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_support)

        btnBack.onClick {
            finish()
        }

        btnCreateTicket.onClick {
            //TODO: Validate fields
            val subject = etSubject.text.toString()
            val comment = etComment.text.toString()

            //TODO: Add progress indicator
            App.createSupportTicket(subject, comment) { success, error ->
                runOnUiThread {
                    if (success) {
                        CustomDialog(
                            this,
                            CustomDialog.DialogIcon.SUCCESS,
                            getString(R.string.support_ticket_success_title),
                            getString(R.string.support_ticket_success_message)
                        )
                            .addAction(
                                CustomDialog.CustomDialogAction(
                                    "Continuar",
                                    CustomDialog.CustomDialogAction.ActionType.POSITIVE
                                ) { dialog ->
                                    dialog.dismiss()
                                    finish()
                                }).show()
                    } else {
                        Log.e("SUPPORT TICKET ERROR", error ?: "Unknown error")
                        CustomDialog(
                            this,
                            CustomDialog.DialogIcon.ERROR,
                            getString(R.string.support_ticket_error_title),
                            error ?: "Ocurrio un error y no fue posible crear el ticket de soporte."
                        )
                            .addAction(
                                CustomDialog.CustomDialogAction(
                                    "Ok",
                                    CustomDialog.CustomDialogAction.ActionType.NEGATIVE
                                ) { dialog ->
                                    dialog.dismiss()
                                    finish()
                                }).show()
                    }
                }
            }
        }
    }
}