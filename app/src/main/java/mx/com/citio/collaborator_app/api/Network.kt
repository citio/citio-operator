package mx.com.citio.collaborator_app.api

import android.app.Application
import android.util.Log
import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.api.cache.http.HttpCachePolicy
import com.apollographql.apollo.cache.http.ApolloHttpCache
import com.apollographql.apollo.cache.http.DiskLruHttpCacheStore
import mx.com.citio.collaborator_app.BuildConfig
import okhttp3.OkHttpClient
import java.io.File

object Network {
    lateinit var apollo: ApolloClient

    fun init(context: Application) {
        val file = File(context.filesDir, "apolloCache")
        val size: Long = 1024 * 1024
        val cacheStore = DiskLruHttpCacheStore(file, size)

        val okHttpClient = OkHttpClient.Builder()
            .addNetworkInterceptor { chain ->
                val request = chain.request().newBuilder()
                    .addHeader("Authorization", "bearer ${App.authorization ?: ""}")
                    .build()
                chain.proceed(request)
            }
            .build()

        apollo = ApolloClient.builder()
            .serverUrl(BuildConfig.GRAPH_SERVER_URL)
            .httpCache(ApolloHttpCache(cacheStore))
            .defaultHttpCachePolicy(HttpCachePolicy.CACHE_FIRST)
            .okHttpClient(okHttpClient)
            .logger { priority, message, t, args -> Log.e("APOLLO LOGGER", message) }
            .build()
    }
}