package mx.com.citio.collaborator_app.components

import android.app.AlertDialog
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.squareup.picasso.Picasso
import mx.com.citio.collaborator_app.R
import mx.com.citio.collaborator_app.classes.onClick

class BarcodeDialog(context: Context, arrReference: List<String>, arrBarcodeUrl: List<String>, callback: (dialog: BarcodeDialog) -> Unit) : AlertDialog(context) {


    init {

        val view = layoutInflater.inflate(R.layout.dialog_barcode3, null)
        val btnPositive: Button = view.findViewById(R.id.btnPositive)

        dialogPaynet(view, arrReference, arrBarcodeUrl)
        dialogOxxo(view, arrReference, arrBarcodeUrl)

        btnPositive.onClick {
            callback(this)
        }

        this.setView(view)
        this.setCancelable(false)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window?.attributes?.windowAnimations = R.style.SlideFromDown
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

    }

    fun dialogPaynet(view:View, arrReference:List<String>, arrBarcodeUrl:List<String>){
        val tvReferencePaynet: TextView = view.findViewById(R.id.tvReferencePaynet)
        val ivBarcodePaynet: ImageView = view.findViewById(R.id.ivBarcodePaynet)
        val btnCopyPaynet: Button = view.findViewById(R.id.btnCopyPaynet)

        val referencePaynet= arrReference[0]
        tvReferencePaynet.text = referencePaynet

        btnCopyPaynet.onClick {
            val clipboardManager = context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clipData = ClipData.newPlainText("text", "Tu referencia para pago en efectivo: ${referencePaynet}")
            clipboardManager.setPrimaryClip(clipData)

            Toast.makeText(context, "Copiado en portapapeles", Toast.LENGTH_LONG).show()
        }

        Picasso.get()
            .load(arrBarcodeUrl[0])
            .into(ivBarcodePaynet)
    }

    fun dialogOxxo(view:View, arrReference:List<String>, arrBarcodeUrl:List<String>){
        val tvReferenceOxxo: TextView = view.findViewById(R.id.tvReferenceOxxo)
        val ivBarcodeOxxo: ImageView = view.findViewById(R.id.ivBarcodeOxxo)
        val btnCopyOxxo: Button = view.findViewById(R.id.btnCopyOxxo)

        val referenceOxxo= arrReference[1]
        tvReferenceOxxo.text = referenceOxxo

        tvReferenceOxxo.text = referenceOxxo
        btnCopyOxxo.onClick {
            val clipboardManager = context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clipData = ClipData.newPlainText("text", "Tu referencia para pago en efectivo: ${referenceOxxo}")
            clipboardManager.setPrimaryClip(clipData)

            Toast.makeText(context, "Copiado en portapapeles", Toast.LENGTH_LONG).show()
        }

        Picasso.get()
            .load(arrBarcodeUrl[1])
            .into(ivBarcodeOxxo)
    }

}