package mx.com.citio.collaborator_app.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.google.android.gms.maps.model.LatLng
import mx.com.citio.collaborator_app.R
import mx.com.citio.collaborator_app.api.Station
import mx.com.citio.collaborator_app.api.availableBatteries
import mx.com.citio.collaborator_app.api.distanceFrom

class StationAdapter(context: Context, val layout: Int, val list: List<Station>, val currentLocation: LatLng?, val callback: (station: Station) -> Unit) :
    ArrayAdapter<Station>(context, layout, list) {

    val ctx: Context = context

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val v = View.inflate(context, layout, null)

        val station = list[position]

        val tvStationName = v.findViewById<TextView>(R.id.tvStationName)
        val tvAddress = v.findViewById<TextView>(R.id.tvAddress)
        val tvPostalCode = v.findViewById<TextView>(R.id.tvPostalCode)
        val tvDistance = v.findViewById<TextView>(R.id.tvDistance)

        v.findViewById<ImageView>(R.id.ivAvailable).run {
            if (station.availableBatteries > 0)
                setImageResource(R.drawable.ic_tick)
            else
                setImageResource(R.drawable.ic_close)
        }

        tvStationName.text = station.shortName
        tvAddress.text = station.address?.streetName ?: "Calle #ext, Colonia"
        tvPostalCode.text = station.address?.zipcode ?: "C.P. -"
        tvDistance.text = station.distanceFrom(currentLocation)

        v.isClickable = true
        v.setOnClickListener { callback(station) }

        return v
    }
}