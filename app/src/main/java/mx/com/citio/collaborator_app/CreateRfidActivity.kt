package mx.com.citio.collaborator_app

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.nfc.NfcAdapter
import android.nfc.NfcManager
import android.nfc.Tag
import android.nfc.tech.IsoDep
import android.nfc.tech.NfcA
import android.nfc.tech.NfcB
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_create_rfid.*
import kotlinx.android.synthetic.main.drawer.*
import mx.com.citio.collaborator_app.api.App
import mx.com.citio.collaborator_app.classes.onClick
import mx.com.citio.collaborator_app.components.ConfirmExchangeQR
import mx.com.citio.collaborator_app.components.CustomDialog


class CreateRfidActivity : AppCompatActivity(){

    private var targetActivity: Class<*>? = null
    private var nfcAdapter: NfcAdapter? = null
    private var intentFiltersArray: Array<IntentFilter>? = null
    private var techListsArray: Array<Array<String>>? = null
    private var pendingIntent: PendingIntent? = null

    var serialNumber :String? = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_rfid)
        setContentView(R.layout.activity_create_rfid)
        supportActionBar?.hide()

        btnBack.onClick { onBackPressed() }

        btnRegisterRfid.onClick { registerRfid() }

        println("Tap Tag window ready ...")

        val nfcManager = getSystemService(Context.NFC_SERVICE) as NfcManager
//        nfcAdapter = nfcManager.getDefaultAdapter();
        //        nfcAdapter = nfcManager.getDefaultAdapter();
        nfcAdapter = NfcAdapter.getDefaultAdapter(this)

        if (nfcManager != null) {
            println("NFC Manager ready ...")
        }

        if (nfcAdapter != null) {
            println("NFC Adapter ready ...")
        }

        pendingIntent = PendingIntent.getActivity(
            this, 0, Intent(this, javaClass).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0
        )
        intentFiltersArray = arrayOf(IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED))
        techListsArray = arrayOf(
            arrayOf(NfcA::class.java.name), arrayOf(
                NfcB::class.java.name
            ), arrayOf(IsoDep::class.java.name)
        )
    }


    override fun onBackPressed() {
        super.onBackPressed()

        val intent = Intent(this@CreateRfidActivity, HomeActivity::class.java)
        startActivity(intent)
        finish()

    }



    private fun registerRfid(){
         val rfidAlias =  etRfidAlias.text.toString()
          runOnUiThread {
                   ConfirmExchangeQR(this, getString(R.string.register_rfid),"¿Deseas crear el RFID ${serialNumber}?")
                       .addAction(ConfirmExchangeQR.CustomDialogAction(getString(R.string.register), ConfirmExchangeQR.CustomDialogAction.ActionType.POSITIVE) { dialog ->

                           progress.visibility = View.VISIBLE

                           serialNumber?.let {
                               App.createRfid(it, rfidAlias) { success, error ->
                                   error?.let {
                                       //If error
                                       runOnUiThread {
                                           CustomDialog(
                                               this,
                                               CustomDialog.DialogIcon.ERROR,
                                               "¡ERROR!",
                                               error
                                           )
                                               .addAction(
                                                   CustomDialog.CustomDialogAction(
                                                       "Aceptar",
                                                       CustomDialog.CustomDialogAction.ActionType.POSITIVE
                                                   ) { dialog ->
                                                       rlEmptyRfid.visibility = View.VISIBLE
                                                       llRfidData.visibility = View.GONE
                                                       etRfidAlias.setText(null)
                                                       dialog.dismiss()

                                                   }).show()

                                           progress.visibility = View.GONE
                                       }
                                   }

                                   if (success)  {
                                       runOnUiThread {
                                           CustomDialog(
                                               this,
                                               CustomDialog.DialogIcon.SUCCESS,
                                               "¡Listo!",
                                               "El RFID ha sido creado con éxito"
                                           )
                                               .addAction(
                                                   CustomDialog.CustomDialogAction(
                                                       "Aceptar",
                                                       CustomDialog.CustomDialogAction.ActionType.POSITIVE
                                                   ) { dialog ->
                                                       rlEmptyRfid.visibility = View.VISIBLE
                                                       llRfidData.visibility = View.GONE
                                                       etRfidAlias.setText(null)
                                                       dialog.dismiss()
                                                       //finish()
                                                   }).show()

                                           progress.visibility = View.GONE
                                       }

                                   }

                               }
                           }


                           dialog.dismiss()
                       })
                       .addAction(ConfirmExchangeQR.CustomDialogAction("CANCELAR", ConfirmExchangeQR.CustomDialogAction.ActionType.NEGATIVE) { dialog ->
                           Toast.makeText(this, "Cancelado" +
                                   "", Toast.LENGTH_SHORT).show()
                           dialog.dismiss()
                       })
                       .show()

               }

        }


    override fun onResume() {
        super.onResume()
        val intent = Intent()
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
        val nfcPendingIntent = PendingIntent.getActivity(this, 0, intent, 0)
        if (nfcAdapter != null) {
            nfcAdapter!!.enableForegroundDispatch(
                this,
                pendingIntent,
                intentFiltersArray,
                techListsArray
            )
        }
    }

    override fun onPause() {
        super.onPause()
        if (nfcAdapter != null) {
            try {
                nfcAdapter!!.disableForegroundDispatch(this)
            } catch (ex: IllegalStateException) {
                Log.e("ATHTAG", "Error disabling NFC foreground dispatch", ex)
            }
        }
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        println("Doing onNewIntent() ...")

          val SELECT = byteArrayOf(
              0x00.toByte(),  // CLA Class
              0xA4.toByte(),  // INS Instruction
              0x04.toByte(),  // P1  Parameter 1
              0x00.toByte(),  // P2  Parameter 2
              0x0A.toByte(),  // Length
              0x63, 0x64, 0x63, 0x00, 0x00, 0x00, 0x00, 0x32, 0x32, 0x31 // AID
          )

        val GET_STRING = byteArrayOf(
            0x80.toByte(), // CLA Class
            0x04, // INS Instruction
            0x00, // P1  Parameter 1
            0x00, // P2  Parameter 2
            0x10  // LE  maximal number of bytes expected in result
        )

        // Use NFC to read tag
        val tag: Tag? = intent?.getParcelableExtra(NfcAdapter.EXTRA_TAG)
        //val nfc = NfcB.get(tag)
        //val isoTag = IsoDep.get(tag)
        //Log.e("tag", "$tag")

        if (tag != null) {
            Log.e("tagID", "${tag.id}")
            Log.e("BYTESTOHEX1", "${bytesToHex(tag.id)}")
             serialNumber = bytesToHex(tag.id)
            Log.e("BYTESTOHEXserialNumber", "${serialNumber}")
            //tvEmpty.text = serialNumber
                val string = String(tag.id, Charsets.UTF_16)
            val string1 = String(tag.id, Charsets.UTF_8)

            Log.e("string", "${string}")
            Log.e("string1", "${string1}")

            if(serialNumber !== null){
                rlEmptyRfid.visibility = View.GONE
                llRfidData.visibility = View.VISIBLE
                valueSerialNumber.text = serialNumber


            }else{
                runOnUiThread {
                    CustomDialog(
                        this,
                        CustomDialog.DialogIcon.ERROR,
                        "¡ERROR!",
                        "Error al detectar RFID, intenta de nuevo"
                    )
                        .addAction(
                            CustomDialog.CustomDialogAction(
                                "Aceptar",
                                CustomDialog.CustomDialogAction.ActionType.POSITIVE
                            ) { dialog ->
                                dialog.dismiss()
                            }).show()
                }
            }

        }else{
            runOnUiThread {
                Toast.makeText(this, "No se detectó un RFID en la tarjeta", Toast.LENGTH_SHORT).show()
            }

        }

       // Log.i("tagNFCA", "${NfcA.get(tag)}")
        //Log.i("tagISO", "${isoTag}")
       // Log.i("ISOHIla", "${isoTag.hiLayerResponse}")

        }

    private val hexArray = "0123456789ABCDEF".toCharArray()
    fun bytesToHex(bytes: ByteArray?): String? {
        if (bytes == null) return null
        val hexChars = CharArray(bytes.size * 2)
        for (j in bytes.indices) {

            val v: Int = bytes.get(j).toInt() and 0xFF
            hexChars[j * 2] = hexArray.get(v ushr 4)
            hexChars[j * 2 + 1] = hexArray.get(v and 0x0F)
        }
        return String(hexChars)
    }


    fun setTargetRedirectIntent(activity: Class<*>) {
        targetActivity = activity
    }
}