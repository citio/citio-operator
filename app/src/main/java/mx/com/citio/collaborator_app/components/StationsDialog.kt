package mx.com.citio.collaborator_app.components

import android.app.AlertDialog
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.view.Window
import android.widget.*
import com.google.android.gms.maps.model.LatLng
import mx.com.citio.collaborator_app.R
import mx.com.citio.collaborator_app.adapters.StationAdapter
import mx.com.citio.collaborator_app.api.Station
import mx.com.citio.collaborator_app.api.floatDistanceFrom
import mx.com.citio.collaborator_app.api.isFavorite
import mx.com.citio.collaborator_app.classes.dp
import mx.com.citio.collaborator_app.classes.onClick
import mx.com.citio.type.Zones


class StationsDialog(context: Context, val stations: ArrayList<Station>, private val currentLocation: LatLng?, callback: (dialog: StationsDialog, station: Station) -> Unit) : AlertDialog(context) {
    private var filteredStations: ArrayList<Station> = ArrayList()

    private var lvStations: ListView
    private var btnStations: Button
    private var btnFavourites: Button

    private var containerAreaSelector: RelativeLayout
    private var tvAllAreas: TextView
    private var rlAreaSelector: RelativeLayout
    private var llAreas: LinearLayout
    private var tvSelectedZone: TextView

    private var selectedZone: Zones? = null

    init {
        val view = layoutInflater.inflate(R.layout.dialog_stations, null)

        btnStations = view.findViewById(R.id.btnStations)
        btnFavourites = view.findViewById(R.id.btnFavourites)
        val ibClose: ImageButton = view.findViewById(R.id.ibClose)
        lvStations = view.findViewById(R.id.listView)

        tvAllAreas = view.findViewById(R.id.tvAllAreas)
        containerAreaSelector = view.findViewById(R.id.containerAreaSelector)
        rlAreaSelector = view.findViewById(R.id.rlAreaSelector)
        llAreas = view.findViewById(R.id.llAreas)

        tvSelectedZone = view.findViewById(R.id.tvSelectedZone)

        Zones.values().filter { it.name != "UNKNOWN__" }.forEach { zone ->
            Log.w("ZONE", zone.name)

            val tv = TextView(context)
            tv.text = zone.name.toUpperCase()
            tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12f)
            tv.setPadding(12f.dp(context), 6f.dp(context), 6f.dp(context), 6f.dp(context))

            tv.onClick {
                Log.w("FILTER", "Filtering zone: ${zone.name}")
                selectedZone = zone
                updateList(false)
                toggleAreaSelector()
            }

            llAreas.addView(tv)
        }

        tvAllAreas.onClick {
            selectedZone = null
            updateList(false)
            toggleAreaSelector()
        }

        lvStations.adapter = StationAdapter(context, R.layout.item_station, filteredStations, currentLocation) {
            callback(this, it)
        }

        ibClose.setOnClickListener { dismiss() }

        this.setView(view)
        this.setCancelable(true)

        btnFavourites.onClick {
            updateList(true)
            if (llAreas.visibility == View.VISIBLE)
                llAreas.visibility = View.GONE
        }

        btnStations.onClick {
            updateList(false)
        }

        rlAreaSelector.onClick {
            toggleAreaSelector()
        }

        updateList(false)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window?.attributes?.windowAnimations = R.style.SlideFromDown
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    private fun toggleAreaSelector() {
        llAreas.visibility = if (llAreas.visibility == View.VISIBLE) View.GONE else View.VISIBLE
    }

    private fun updateList(favs: Boolean) {
        tvSelectedZone.text = selectedZone?.name ?: "MÁS CERCANAS"
        filteredStations.clear()

        if (favs) {
            containerAreaSelector.visibility = View.GONE

            btnStations.backgroundTintList = ColorStateList.valueOf(context.getColor(android.R.color.white))
            btnStations.setTextColor(context.getColor(R.color.darkGray))

            btnFavourites.backgroundTintList = ColorStateList.valueOf(context.getColor(R.color.colorAccent))
            btnFavourites.setTextColor(context.getColor(R.color.colorPrimaryDark))

            filteredStations.addAll(stations.filter { it.isFavorite })
        } else {
            containerAreaSelector.visibility = View.VISIBLE

            btnStations.backgroundTintList = ColorStateList.valueOf(context.getColor(R.color.colorAccent))
            btnStations.setTextColor(context.getColor(R.color.colorPrimaryDark))

            btnFavourites.backgroundTintList = ColorStateList.valueOf(context.getColor(android.R.color.white))
            btnFavourites.setTextColor(context.getColor(R.color.darkGray))

            filteredStations.addAll(stations.filter { if (selectedZone != null) it.zone == selectedZone else true })
        }

        filteredStations.sortBy { it.floatDistanceFrom(currentLocation) }
        (lvStations.adapter as StationAdapter).notifyDataSetChanged()
    }
}