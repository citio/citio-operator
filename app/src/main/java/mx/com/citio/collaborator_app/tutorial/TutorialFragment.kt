package mx.com.citio.collaborator_app.tutorial

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_tutorial.*
import mx.com.citio.collaborator_app.R

class TutorialFragment : Fragment() {
    var icon = 0
    var title: String? = null
    var content: String? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        arguments?.run {
            icon = getInt("icon", -1)
            title = getString("title")
            content = getString("content")
        }

        return layoutInflater.inflate(R.layout.fragment_tutorial, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ivIcon.setImageResource(icon)
        tvTitle.text = title
    }

    companion object {
        fun newInstance(icon: Int, title: String, content: String): TutorialFragment {
            val fragment = TutorialFragment()

            fragment.arguments = Bundle().apply {
                putInt("icon", icon)
                putString("title", title)
                putString("content", content)
            }

            return fragment
        }
    }
}