package mx.com.citio.collaborator_app.api

import android.content.Context
import android.util.Log
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Input
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.api.cache.http.HttpCachePolicy
import com.apollographql.apollo.exception.ApolloException
import mx.com.citio.*
import mx.com.citio.collaborator_app.BuildConfig
import mx.com.citio.collaborator_app.classes.SessionManager
import mx.com.citio.type.EndpointType
import mx.com.citio.type.NotificationGroupType
import mx.com.citio.type.ParamsInput
import mx.com.citio.type.SortDirectionType
import org.json.JSONStringer

typealias User = FetchUserQuery.Me
typealias Station = GetStationsQuery.Data1
typealias Faq = GetFaqsQuery.Data1
typealias Rfid = RfidInscriptionMutation.RfidInscription
typealias CitioNotification = GetNotificationsQuery.Data1
typealias CitioAlarm = GetAlarmsQuery.Data1
typealias Rfids = GetrfidAutocompleteQuery.Data
typealias Batteries = BatteryAutocompleteQuery.Data


typealias AlarmsPage = GetAlarmsQuery.Data
typealias PaymentsPage = GetPaymentsQuery.Data
typealias Payment = GetPaymentsQuery.Data1
typealias PaymentMethod = GetPaymentMethodsQuery.Data1
typealias DefaultPaymentMethod = GetDefaultPaymentMethodQuery.PaymentFavorite

typealias OrdersPage = GetOrdersQuery.Data
typealias Order = GetOrdersQuery.Data1

typealias Plan = GetPlansQuery.Data1

object App {
    const val signupUrl = BuildConfig.SIGNUP_URL
    const val forgotPasswordUrl = BuildConfig.FORGOTPASSWORD_URL



    object Contact {
        const val whatsapp = "+5215621684499"
        const val phone = "55 8686 5410"
        const val email = "soporte@citio.co"
    }

    var authorization: String? = null
    var currentUser: User? = null

    var searchBattery: String? = null


    fun createUser(name: String, lastname: String,motherlastname: String,birthday: String,email: String,username: String,password: String,confirmPassword: String,zipCode: String,phone: String,reference: String?, callback: (success: Boolean, error: String?) -> Unit) {
        val mutation = CreateUserMutation(name, lastname,motherlastname,birthday, email, username, password, confirmPassword, zipCode, phone, Input.optional(reference))

        Network.apollo.mutate(mutation).enqueue(object : ApolloCall.Callback<CreateUserMutation.Data>() {
            override fun onFailure(e: ApolloException) {
                e.printStackTrace()
                Log.e("CREATEUSER", e.localizedMessage)
            }

            override fun onResponse(response: Response<CreateUserMutation.Data>) {
                response.data()?.createUser?.let {
                    Log.e("CREATEUSER", "SUCCESS $it")
                        callback(true, null)
                    } ?: run {
                    val errors = response.errors().map { it.message() }.joinToString(", ")
                    Log.e("CREATEUSER ERR", errors)
                    callback(false, errors)
                }
            }
        })
    }


    fun login(context: Context, username: String, password: String, callback: (user: User?, error: String?) -> Unit) {
        val mutation = LoginMutation(username, password, Input.optional(true))

        Network.apollo.mutate(mutation).enqueue(object : ApolloCall.Callback<LoginMutation.Data>() {
            override fun onFailure(e: ApolloException) {
                e.printStackTrace()
                Log.e("LOGIN", e.localizedMessage)
            }

            override fun onResponse(response: Response<LoginMutation.Data>) {
                response.data()?.login?.let {
                    Log.i("LOGININFO", "$it")
                    Log.e("TOKEN EXP", "${it.tokenExpiration}")
                    val token = it.token
                    authorization = token
                    SessionManager.saveSession(context, token)
                    fetchUser(callback)
                } ?: run {
                    val errors = response.errors().map { it.message() }.joinToString(", ")
                    Log.e("LOGINERR", errors)
                    callback(null, errors)
                }
            }
        })
    }

    fun fetchUser(callback: (user: User?, error: String?) -> Unit) {
        Log.i("fetchUser", "authorization $authorization")
        val query = FetchUserQuery()

        Network.apollo
            .query(query)
            .httpCachePolicy(HttpCachePolicy.NETWORK_ONLY)
            .enqueue(object : ApolloCall.Callback<FetchUserQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.e("FETCH USER", e.localizedMessage)
                    callback(null, e.localizedMessage)
                }

                override fun onResponse(response: Response<FetchUserQuery.Data>) {
                    response.data()?.me?.let {
                        Log.i("fetchUser", "fetchUserData $it")
                        currentUser = it
                        callback(it, null)
                    } ?: run {
                        val errors = response.errors().map { it.message() }.joinToString(", ")
                        Log.e("FETCH USER", errors)
                        callback(null, errors)
                    }
                }
            })
    }




    fun getDefaultPaymentMethod(callback: (paymentFavorite: DefaultPaymentMethod?, error: String?) -> Unit) {
        val query = GetDefaultPaymentMethodQuery()

        Network.apollo
            .query(query)
            .httpCachePolicy(HttpCachePolicy.NETWORK_ONLY)
            .enqueue(object : ApolloCall.Callback<GetDefaultPaymentMethodQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.e("DEFAULT PM", e.localizedMessage)
                    callback(null, e.localizedMessage)
                }

                override fun onResponse(response: Response<GetDefaultPaymentMethodQuery.Data>) {
                    response.data()?.paymentFavorite?.let {
                        callback(it, null)
                    } ?: run {
                        val errors = response.errors().map { it.message() }.joinToString(", ")
                        Log.e("NOTIFICATIONS", errors)
                        callback(null, errors)
                    }
                }
            })
    }


    fun assignMeBattery(batteriesIds: ArrayList<String>?,operatorId: String, batteries: ArrayList<String>?, callback: (success: Boolean, error: String?) -> Unit) {
        val mutation = if (batteriesIds !== null ){ AssignBatteriesToOperatorMutation(Input.optional(batteriesIds), operatorId, Input.absent() ) } else { AssignBatteriesToOperatorMutation(Input.absent(), operatorId, Input.optional(batteries) )}
        Log.i("APP", "AssignBatteryToOpe $batteriesIds, $operatorId, $batteries")

        Network.apollo.mutate(mutation)
            .enqueue(object : ApolloCall.Callback<AssignBatteriesToOperatorMutation.Data>() {
                override fun onResponse(response: Response<AssignBatteriesToOperatorMutation.Data>) {
                    response.data()?.assignBatteriesToOperator?.let {
                        callback(true, null)
                    } ?: run {
                        val errors = response.errors().map { it.message() }.joinToString(", ")
                        Log.e("ASSIGNBATOPERAT ERR", errors)
                        callback(false, errors)
                    }
                }

                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.e("SET ASSIGNBATOPERAT ERR", e.localizedMessage)
                }
            })
    }

    fun setDefaultPaymentMethod(defaultPaymentMethod: DefaultPaymentMethod, callback: (success: Boolean, error: String?) -> Unit) {
        val mutation = if (defaultPaymentMethod.paymentMethodId !== null) CreatePaymentFavoriteMutation(Input.optional(defaultPaymentMethod.paymentMethodId)) else  CreatePaymentFavoriteMutation(Input.optional(null))
        Log.i("APPDEFAULMETH", "DEFAULMET: ${defaultPaymentMethod.paymentMethodId}")
        
        Network.apollo.mutate(mutation)
            .enqueue(object : ApolloCall.Callback<CreatePaymentFavoriteMutation.Data>() {
                override fun onResponse(response: Response<CreatePaymentFavoriteMutation.Data>) {
                    response.data()?.createPaymentFavorite?.let {
                        Log.i("APPDEFAULMETH", "SUCESSDEFAULMET: $it")
                        callback(true, null)
                    } ?: run {
                        val errors = response.errors().map { it.message() }.joinToString(", ")
                        Log.e("SET DEFAULT PAYMENT", errors)
                        callback(false, errors)
                    }
                }

                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.e("SET DEFAULT PAYMENT", e.localizedMessage)
                }
            })
    }




    //---------------------------------------  Exchanges and subscriptions -----------------------------------

    fun payExchangeCard(rfid: String, paymentMethodId: String,exchanges:Int,deviceSessionId:String?, callback: (payment: PayExchangeCardMutation.PayExchangeCard?, error: String?) -> Unit) {
        val mutation = PayExchangeCardMutation(rfid, paymentMethodId, exchanges, Input.optional(deviceSessionId))
        Log.i("APP", "ExchangeCard $rfid, $deviceSessionId,$paymentMethodId, $exchanges")

        Network.apollo.mutate(mutation)
            .enqueue(object : ApolloCall.Callback<PayExchangeCardMutation.Data>() {
                override fun onResponse(response: Response<PayExchangeCardMutation.Data>) {
                    response.data()?.payExchangeCard?.let { payment ->
                        callback(payment, null)
                    } ?: run {
                        val errors = response.errors().map { it.message() }.joinToString(", ")
                        Log.e("PAY EXCHANGE CARD", errors)
                        callback(null, errors)
                    }
                }

                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.e("SET DEFAULT PAYMENT", e.localizedMessage)
                }
            })
    }

    fun payExchangeConektaOxxo(rfid: String, callback: (payment: ExchangeConektaOxxoMutation.PayExchangeConektaOxxo?, error: String?) -> Unit) {
        val mutation = ExchangeConektaOxxoMutation(rfid)

        Network.apollo.mutate(mutation)
            .enqueue(object : ApolloCall.Callback<ExchangeConektaOxxoMutation.Data>() {
                override fun onResponse(response: Response<ExchangeConektaOxxoMutation.Data>) {
                    response.data()?.payExchangeConektaOxxo?.let { payment ->
                        callback(payment, null)
                    } ?: run {
                        val errors = response.errors().map { it.message() }.joinToString(", ")
                        Log.e("PAY EXCHANGE CON_OXXO", errors)
                        callback(null, errors)
                    }
                }

                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.e("SET DEFAULT PAYMENT", e.localizedMessage)
                }
            })
    }

    fun payExchangeCash(rfid: String, exchanges:Int, callback: (payment: PayExchangeCashMutation.PayExchangeCash?, error: String?) -> Unit) {
        val mutation = PayExchangeCashMutation(rfid,exchanges)
        Log.i("APP", "ExchangeCard $rfid, $exchanges")
        Network.apollo.mutate(mutation)
            .enqueue(object : ApolloCall.Callback<PayExchangeCashMutation.Data>() {
                override fun onResponse(response: Response<PayExchangeCashMutation.Data>) {
                    response.data()?.payExchangeCash?.let { payment ->
                        callback(payment, null)
                    } ?: run {
                        val errors = response.errors().map { it.message() }.joinToString(", ")
                        Log.e("PAY EXCHANGE CON_CASH", errors)
                        callback(null, errors)
                    }
                }

                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.e("SET DEFAULT PAYMENT", e.localizedMessage)
                }
            })
    }


    fun paySubscriptionCard(rfid: String, paymentMethodId: String, planId: String, callback: (payment: SubscriptionCardMutation.SubscriptionCard?, error: String?) -> Unit) {
    val mutation = SubscriptionCardMutation(rfid, paymentMethodId, Input.optional(planId))
    Log.i("APPSUBS","SUBSCARD:$rfid, $paymentMethodId, $planId")
        Network.apollo.mutate(mutation)
            .enqueue(object : ApolloCall.Callback<SubscriptionCardMutation.Data>() {
                override fun onResponse(response: Response<SubscriptionCardMutation.Data>) {
                    response.data()?.subscriptionCard?.let { payment ->
                        callback(payment, null)
                    } ?: run {
                        val errors = response.errors().map { it.message() }.joinToString(", ")
                        Log.e("PAY SUBS CONEKTA", errors)
                        callback(null, errors)
                    }
                }

                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.e("SET DEFAULT PAYMENT", e.localizedMessage)
                }
            })
    }

    //---------------------------------------  Exchanges with QR -----------------------------------

    fun createQrOrderData(rfidId: String, stationId: String, callback: (success: Boolean, error: String?) -> Unit) {
        val mutation = CreateQrOrderDataMutation(rfidId, stationId)

        Network.apollo.mutate(mutation)
            .enqueue(object : ApolloCall.Callback<CreateQrOrderDataMutation.Data>() {
                override fun onResponse(response: Response<CreateQrOrderDataMutation.Data>) {
                    response.data()?.createQrOrder?.let {
                        callback(it, null)
                    } ?: run {
                        val errors = response.errors().map { it.message() }.joinToString(", ")
                        Log.e("CREATE ORDER QR ERRORS", errors)
                        callback(false, errors)
                    }
                }
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.e("CREATE ORDER QR","ERRORSEE $e, ${e.localizedMessage}")
                    callback(false, e.localizedMessage)
                }
            })
    }

    fun executeQrOrderData(rfidId: String, stationId: String, qrCodeId: String, callback: (success: Boolean, error: String?) -> Unit) {
        val mutation = ExecuteQrOrderMutation(rfidId, stationId, qrCodeId)

        Network.apollo.mutate(mutation)
            .enqueue(object : ApolloCall.Callback<ExecuteQrOrderMutation.Data>() {
                override fun onResponse(response: Response<ExecuteQrOrderMutation.Data>) {
                    response.data()?.executeQrOrder?.let {
                        callback(it, null)
                    } ?: run {
                        val errors = response.errors().map { it.message() }.joinToString(", ")
                        Log.e("EXECUTE ORDER QR ERRORS", errors)
                        callback(false, errors)
                    }
                }

                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.e("EXECUTE QR","ERRORSEE $e, ${e.localizedMessage}")
                    callback(false, e.localizedMessage)
                }


            })
    }

    //------------------------------------ User data and actions -----------------------------------
    fun loadNotifications(callback: (notifications: List<CitioNotification>) -> Unit) {
        val params =ParamsInput(Input.optional(0), Input.optional(20), Input.absent(),Input.absent(), Input.optional(SortDirectionType.DESC))
        val query = GetNotificationsQuery(Input.optional(params))

        Network.apollo
            .query(query)
            .httpCachePolicy(HttpCachePolicy.NETWORK_ONLY)
            .enqueue(object : ApolloCall.Callback<GetNotificationsQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.e("NOTIFICATIONS", e.localizedMessage)
                }

                override fun onResponse(response: Response<GetNotificationsQuery.Data>) {
                    response.data()?.myNotifications?.data?.let {
                        callback(it)
                    } ?: run {
                        val errors = response.errors().map { it.message() }.joinToString(", ")
                        Log.e("NOTIFICATIONS", errors)
                        callback(ArrayList())
                    }
                }
            })
    }


    fun loadAlarms(page: Int, limit: Int,callback: (page: AlarmsPage?) -> Unit) {
        val params =ParamsInput(Input.optional(page), Input.optional(limit), Input.absent(),Input.absent(), Input.optional(SortDirectionType.DESC))
        val query = GetAlarmsQuery(Input.optional(params))

        Network.apollo
            .query(query)
            .httpCachePolicy(HttpCachePolicy.NETWORK_ONLY)
            .enqueue(object : ApolloCall.Callback<GetAlarmsQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.e("APP ALARMS", e.localizedMessage)
                }

                override fun onResponse(response: Response<GetAlarmsQuery.Data>) {
                    response.data()?.let {
                        callback(it)
                    } ?: run {
                        val errors = response.errors().map { it.message() }.joinToString(", ")
                        Log.e("APP ALARMS ERROR", errors)
                        callback(null)
                    }
                }
            })
    }

    fun readNotification(notification: CitioNotification, callback: (success: Boolean, error: String?) -> Unit) {
        val mutation = ReadNotificationMutation(notification.id)

        Network.apollo.mutate(mutation)
            .enqueue(object : ApolloCall.Callback<ReadNotificationMutation.Data>() {
                override fun onResponse(response: Response<ReadNotificationMutation.Data>) {
                    response.data()?.readNotification?.let {
                        callback(it, null)
                    } ?: run {
                        val errors = response.errors().map { it.message() }.joinToString(", ")
                        Log.e("LOGIN", errors)
                        callback(false, errors)
                    }
                }

                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.e("LOGIN", e.localizedMessage)
                }
            })
    }

    fun rfidAutocomplete(search:String,callback: (rfids:Rfids?, error: String?) -> Unit) {
        Log.i("rfidAutocomplete", "authorization $search")

        val query = GetrfidAutocompleteQuery(search)

        Network.apollo
            .query(query)
            .httpCachePolicy(HttpCachePolicy.NETWORK_ONLY)
            .enqueue(object : ApolloCall.Callback<GetrfidAutocompleteQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.e("RFID AUTOCOMPLETE", e.localizedMessage)
                    callback(null, e.localizedMessage)
                }

                override fun onResponse(response: Response<GetrfidAutocompleteQuery.Data>) {
                    response.data()?.let {
                        Log.i("RFIDS", "rfids $it")

                        callback(it, null)
                    } ?: run {
                        val errors = response.errors().map { it.message() }.joinToString(", ")
                        Log.e("rfid AUTOCOMPLETE err", errors)
                        callback(null, errors)
                    }
                }
            })
    }

    fun batterieAutocomplete(search:String,callback: (batteries:Batteries?, error: String?) -> Unit) {
        Log.i("batterieAutocomplete", "search $search")
        searchBattery = search
        val query = BatteryAutocompleteQuery(search)

        Network.apollo
            .query(query)
            .httpCachePolicy(HttpCachePolicy.NETWORK_ONLY)
            .enqueue(object : ApolloCall.Callback<BatteryAutocompleteQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.e("Batterie AUTOCOMPLETE", e.localizedMessage)
                    callback(null, e.localizedMessage)
                }

                override fun onResponse(response: Response<BatteryAutocompleteQuery.Data>) {
                    response.data()?.let {
                        Log.i("BATTERIES", "bats $it")

                        callback(it, null)
                    } ?: run {
                        val errors = response.errors().map { it.message() }.joinToString(", ")
                        Log.e("bats AUTOCOMPLETE err", errors)
                        callback(null, errors)
                    }
                }
            })
    }


    fun loadPaymentsPage(rfid: String, page: Int, limit: Int, callback: (page: PaymentsPage?, error: String?) -> Unit) {
        val params =ParamsInput(Input.optional(page), Input.optional(limit), Input.absent(),Input.absent(), Input.optional(SortDirectionType.DESC))
        val query = GetPaymentsQuery(rfid, Input.optional(params))

        Network.apollo
            .query(query)
            .httpCachePolicy(HttpCachePolicy.NETWORK_ONLY)
            .enqueue(object : ApolloCall.Callback<GetPaymentsQuery.Data>() {
                override fun onResponse(response: Response<GetPaymentsQuery.Data>) {
                    response.data()?.let {
                        callback(it, null)
                    } ?: run {
                        val errors = response.errors().map { it.message() }.joinToString(", ")
                        Log.e("PAYMENTS", errors)
                        callback(null, errors)
                    }
                }

                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.e("PAYMENTS", e.localizedMessage)
                    callback(null, e.localizedMessage)
                }
            })
    }

    fun loadOrdersPage(rfid: String, page: Int, limit: Int, callback: (page: OrdersPage?, error: String?) -> Unit) {
        val params =ParamsInput(Input.optional(page), Input.optional(limit), Input.absent(),Input.absent(), Input.optional(SortDirectionType.DESC))
        val query = GetOrdersQuery(rfid, Input.optional(params))

        Network.apollo
            .query(query)
            .httpCachePolicy(HttpCachePolicy.NETWORK_ONLY)
            .enqueue(object : ApolloCall.Callback<GetOrdersQuery.Data>() {
                override fun onResponse(response: Response<GetOrdersQuery.Data>) {
                    response.data()?.let {
                        callback(it, null)
                    } ?: run {
                        val errors = response.errors().map { it.message() }.joinToString(", ")
                        Log.e("ORDERS", errors)
                        callback(null, errors)
                    }
                }

                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.e("ORDERS", e.localizedMessage)
                    callback(null, e.localizedMessage)
                }
            })
    }

    fun toggleStationFavorite(station: Station, callback: (success: Boolean, error: String?) -> Unit) {
        if (station.isFavorite) {
            val mutation = RemoveStationFromFavoritesMutation(station.id)

            Network.apollo.mutate(mutation)
                .enqueue(object : ApolloCall.Callback<RemoveStationFromFavoritesMutation.Data>() {
                    override fun onResponse(response: Response<RemoveStationFromFavoritesMutation.Data>) {
                        response.data()?.deleteFavoriteStation?.let {
                            callback(it, null)
                        } ?: run {
                            val errors = response.errors().map { it.message() }.joinToString(", ")
                            Log.e("LOGIN", errors)
                            callback(false, errors)
                        }
                    }

                    override fun onFailure(e: ApolloException) {
                        e.printStackTrace()
                        Log.e("LOGIN", e.localizedMessage)
                    }
                })
        } else {
            val mutation = AddStationToFavoritesMutation(station.id)

            Network.apollo.mutate(mutation)
                .enqueue(object : ApolloCall.Callback<AddStationToFavoritesMutation.Data>() {
                    override fun onResponse(response: Response<AddStationToFavoritesMutation.Data>) {
                        response.data()?.createFavoriteStation?.let {
                            callback(it, null)
                        } ?: run {
                            val errors = response.errors().map { it.message() }.joinToString(", ")
                            Log.e("LOGIN", errors)
                            callback(false, errors)
                        }
                    }

                    override fun onFailure(e: ApolloException) {
                        e.printStackTrace()
                        Log.e("LOGIN", e.localizedMessage)
                    }
                })
        }
    }

    //----------------------------------------------------------------------------------------------
    fun loadPlans(callback: (plans: List<Plan>) -> Unit) {
        val filterShow = JSONStringer().`object`().key("show").value(true).endObject().toString()
        val params =ParamsInput(Input.optional(0), Input.optional(20), Input.absent(),Input.absent(), Input.optional(SortDirectionType.ASC))
        val query = GetPlansQuery(Input.optional(params),Input.optional(filterShow),Input.optional(true), Input.absent() )

        Network.apollo
            .query(query)
            .httpCachePolicy(HttpCachePolicy.NETWORK_ONLY)
            .enqueue(object : ApolloCall.Callback<GetPlansQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.e("PLANS", e.localizedMessage)
                }

                override fun onResponse(response: Response<GetPlansQuery.Data>) {
                    Log.i("APP", "RESPONSE_PLAN: ${response.data()?.plans?.data!!} ")
                   // callback(response.data()?.plans?.data!!.filter { it.show })
                    callback(response.data()?.plans?.data!!)
                }
            })
    }
    //----------------------------------------------------------------------------------------------

    fun loadStations(callback: (stations: List<Station>) -> Unit) {
        val params =ParamsInput(Input.optional(0), Input.optional(100), Input.absent(),Input.absent(), Input.optional(SortDirectionType.ASC))
        val query = GetStationsQuery(Input.optional(params))

        Network.apollo
            .query(query)
            .httpCachePolicy(HttpCachePolicy.NETWORK_ONLY)
            .enqueue(object : ApolloCall.Callback<GetStationsQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.e("STATIONS", e.localizedMessage)
                }

                override fun onResponse(response: Response<GetStationsQuery.Data>) {
                    callback(response.data()?.stations?.data!!.filter { it.live })
                }
            })
    }

    //--------------------------- Support --------------------------------------------------------
    fun createSupportTicket(subject: String, comment: String, callback: (success: Boolean, error: String?) -> Unit) {
        val mutation = CreateSupportTicketMutation(subject, comment)

        Network.apollo.mutate(mutation)
            .enqueue(object : ApolloCall.Callback<CreateSupportTicketMutation.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.e("SUPPORT TICKET", e.localizedMessage)
                    callback(false, e.localizedMessage)
                }

                override fun onResponse(response: Response<CreateSupportTicketMutation.Data>) {
                    response.data()?.createTicket?.let {
                        callback(it, null)
                    } ?: run {
                        val errors = response.errors().map { it.message() }.joinToString(", ")
                        callback(false, errors)
                    }
                }
            })
    }

    fun loadFaqs(callback: (faqs: List<Faq>) -> Unit) {
        val params =ParamsInput(Input.optional(0), Input.optional(20), Input.absent(),Input.absent(), Input.optional(SortDirectionType.ASC))
        val query = GetFaqsQuery(Input.optional(params))

        Network.apollo
            .query(query)
            .enqueue(object : ApolloCall.Callback<GetFaqsQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.e("FAQS", e.localizedMessage)
                }

                override fun onResponse(response: Response<GetFaqsQuery.Data>) {
                    callback(response.data()?.faqs?.data!!)
                }
            })
    }

    fun getTerms(callback: (currentTerms: String) -> Unit) {
        val query = GetTermsQuery()

        Network.apollo
            .query(query)
            .enqueue(object : ApolloCall.Callback<GetTermsQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.e("TERMS", e.localizedMessage)
                }

                override fun onResponse(response: Response<GetTermsQuery.Data>) {
                    callback(response.data()?.currentTerm?.term ?: "No se encontraron términos y condiciones")
                }
            })
    }

    fun registerDevice(token: String, user: User) {
        Log.i("PUSH", "Registering $token for ${user.id}")
        val mutation = CreateEndPointMutation(token, EndpointType.ANDROID, user.id,Input.optional(NotificationGroupType.OPERATORS) )

        Network.apollo.mutate(mutation)
            .enqueue(object : ApolloCall.Callback<CreateEndPointMutation.Data>() {
                override fun onFailure(e: ApolloException) {
                    Log.e("REGISTER DEVICE ERROR", e.localizedMessage)
                }

                override fun onResponse(response: Response<CreateEndPointMutation.Data>) {
                    response.data()?.createEndPoint?.let {
                        Log.i("SNS ENDPOINT", "Endpoint created ${it.id} ${it.endpointArn}")
                    } ?: run {
                        val errors = response.errors().map { it.message() }.joinToString(", ")
                        Log.e("REGISTER DEVICE ERROR", errors)
                    }
                }
            })
    }


    fun createRfidInscription(serialNumber: String, callback: (rfid: Rfid?, error: String?) -> Unit) {
        Log.i("createRfidInscription", "authorization $authorization")
        val mutation = RfidInscriptionMutation(serialNumber)
        Log.i("RfidInscription", "rfid $serialNumber")
        Network.apollo.mutate(mutation)
            .enqueue(object : ApolloCall.Callback<RfidInscriptionMutation.Data>() {
                override fun onResponse(response: Response<RfidInscriptionMutation.Data>) {
                    response.data()?.rfidInscription?.let { rfid ->
                        callback(rfid, null)
                    } ?: run {
                        val errors = response.errors().map { it.message() }.joinToString(", ")
                        Log.e("RFID INSCRIPTION ERR", errors)
                        callback(null, errors)
                    }
                }

                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.e("ONFAIL RFID INSCRIP", e.localizedMessage)
                }
            })
    }

    fun createRfid(rfid: String, alias: String, callback: (success: Boolean, error: String?) -> Unit) {
        val mutation = CreateRfidMutation(rfid, Input.optional(alias))
        Log.i("APPRFID","CREATE:$rfid, $alias")
        Network.apollo.mutate(mutation)
            .enqueue(object : ApolloCall.Callback<CreateRfidMutation.Data>() {
                override fun onResponse(response: Response<CreateRfidMutation.Data>){
                    response.data()?.createRfid?.let {
                        callback(true, null)
                    } ?: run {
                        val errors = response.errors().map { it.message() }.joinToString(", ")
                        callback(false, errors)
                    }
                }

                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.e("APPCREATERFID", e.localizedMessage)
                }
            })
    }





  fun assignSubRfids( id: String, serialNumbers: ArrayList<String>, callback: (success: Boolean, error: String?) -> Unit) {
        val mutation = SetSubRfidMutation(id, Input.absent(), Input.optional(serialNumbers))

        Log.i("APPSETSUB","PARAMS:,$id, $serialNumbers ")
      Network.apollo.mutate(mutation)
          .enqueue(object : ApolloCall.Callback<SetSubRfidMutation.Data>() {
              override fun onResponse(response: Response<SetSubRfidMutation.Data>){
                  response.data()?.setSubRfid?.let {
                      callback(true, null)
                  } ?: run {
                      val errors = response.errors().map { it.message() }.joinToString(", ")
                      callback(false, errors)
                  }
              }

              override fun onFailure(e: ApolloException) {
                  e.printStackTrace()
                  Log.e("APPCREATERFID", e.localizedMessage)
              }
          })
    }



}