package mx.com.citio.collaborator_app.sections.funds

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_funds.*
import mx.com.citio.FetchUserQuery
import mx.com.citio.collaborator_app.R
import mx.com.citio.collaborator_app.api.App
import mx.com.citio.collaborator_app.api.DefaultPaymentMethod
import mx.com.citio.collaborator_app.classes.onClick
import mx.com.citio.collaborator_app.components.CustomDialog
import mx.com.citio.type.ContractStatuses

class FundsFragment : Fragment() {
    companion object {
        fun newInstance(title: String?): FundsFragment {
            val fragment = FundsFragment()
            val args = Bundle()
            args.putString("title", title)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return layoutInflater.inflate(R.layout.fragment_funds, container, false)
    }

    private var selectedRfid: FetchUserQuery.Rfid? = null
    private var defaultPaymentMethod: DefaultPaymentMethod? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        
        

        var plan = selectedRfid?.inscription?.contract?.plan?.displayName
        var status = selectedRfid?.status?.status

        when (plan) {
            "Libre" -> {
                btnExchangeHidden()
            }
        }
        when(status){
            ContractStatuses.BLOCKED -> btnExchangeHidden()
            ContractStatuses.CANCEL -> btnExchangeHidden()
            ContractStatuses.CLOSE -> btnExchangeHidden()
            ContractStatuses.EXPIRED -> btnExchangeHidden()
            ContractStatuses.WAITING_FOR_DELIVERY_BATTERY -> btnExchangeHidden()
        }


       // btnPaymentMethods.onClick {
           // PaymentMethodsActivity.start(context!!, this)
        //}

        btnChangePlan.onClick {
           // selectedRfid?.let {
             //   TransactionActivity.start(context!!, this, TransactionActivity.TransactionType.PAY_PLAN, it)
            //} ?: run {
              //  showRfidError()
            //}
            //showPaymentMethodError()
        }

        btnPayExchange.onClick {
          //  selectedRfid?.let {
            //    TransactionActivity.start(context!!, this, TransactionActivity.TransactionType.PAY_EXCHANGE, it)
            //} ?: run {
             //   showRfidError()
            //}
            //showPaymentMethodError()
        }

        loadDefaultPaymentMethod()
    }
    private fun btnExchangeHidden(){
        btnPayExchange.visibility=View.GONE
        tvLabelExchanges.visibility=View.GONE
    }
    private fun updateUi() {
        

      /*   defaultPaymentMethod?.let {
            when (it.processor) {
                ProcessorPaymentType.OPENPAY -> {
                    it.paymentMethodId?.let {
                        llCard.visibility = View.VISIBLE
                        rlCash.visibility = View.GONE
                      
                    } ?: run {
                        llCard.visibility = View.GONE
                        rlCash.visibility = View.VISIBLE
                    }
                }
                ProcessorPaymentType.PAYPAL -> Log.i("PAYMENT METHOD", "Paypal")
                ProcessorPaymentType.PAYNET -> {
                    llCard.visibility = View.GONE
                    rlCash.visibility = View.VISIBLE
                }
                ProcessorPaymentType.CONEKTA -> {
                    it.paymentMethodId?.let {
                        llCard.visibility = View.VISIBLE
                       
                    } ?: run {
                        llCard.visibility = View.GONE
                        rlCash.visibility = View.VISIBLE
                    }
                }
                ProcessorPaymentType.UNKNOWN__ -> Log.i("PAYMENT METHOD", "Unknown")
                ProcessorPaymentType.BRAINTREE -> Log.i("PAYMENT METHOD", "Paypal")
            }
        } ?: run {
            llCard.visibility = View.GONE
            rlCash.visibility = View.GONE
        } */
    }

    private fun showRfidError() {
         CustomDialog(requireContext(), CustomDialog.DialogIcon.ERROR, "", "No tienes un RFID vinculado en tu cuenta")
            .addAction(CustomDialog.CustomDialogAction(context?.getString(R.string.label_continue) ?: "Ok", CustomDialog.CustomDialogAction.ActionType.POSITIVE) { dialog ->
                dialog.dismiss()
            }).show()
    }

    private fun showPaymentMethodError() {
        App.getDefaultPaymentMethod { paymentMethod, error ->
            error?.let {
                activity?.runOnUiThread {
                    CustomDialog(context!!, CustomDialog.DialogIcon.ERROR, "Error", error)
                        .addAction(CustomDialog.CustomDialogAction(context?.getString(R.string.label_continue) ?: "Ok", CustomDialog.CustomDialogAction.ActionType.POSITIVE) { dialog ->
                            dialog.dismiss()
                        }).show()
                }
            }

        }
    }

   

    private fun loadDefaultPaymentMethod() {
        App.getDefaultPaymentMethod { paymentMethod, error ->
            /*error?.let {
                activity?.runOnUiThread {
                    CustomDialog(context!!, CustomDialog.DialogIcon.ERROR, "Error obteniendo método de pago favorito", error)
                        .addAction(CustomDialog.CustomDialogAction(context?.getString(R.string.label_continue) ?: "Ok", CustomDialog.CustomDialogAction.ActionType.POSITIVE) { dialog ->
                           dialog.dismiss()
                        }).show()
                }
            }*/

            paymentMethod?.let {
                Log.i("DEFAULT PAYMET Proces", it.processor.toString())
                activity?.runOnUiThread {
                    defaultPaymentMethod = it
                    updateUi()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.i("RESULT", "Request code: $requestCode Result code: $resultCode")

     //   if (requestCode == TransactionActivity.REQUEST_CODE && resultCode == Activity.RESULT_OK) {
       //     //TODO: fetch user and update ui
        //}

       // if (requestCode == PaymentMethodsActivity.REQUEST_CODE && resultCode == Activity.RESULT_OK) {
         //   loadDefaultPaymentMethod()
        //}
    }
}