package mx.com.citio.collaborator_app.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.RelativeLayout
import android.widget.TextView
import mx.com.citio.collaborator_app.R
import mx.com.citio.collaborator_app.classes.format
import mx.com.citio.collaborator_app.classes.moneyFormat
import mx.com.citio.collaborator_app.classes.toDate

class RecordAdapter(context: Context, val layout: Int, val list: List<Record>) : ArrayAdapter<RecordAdapter.Record>(context, layout, list) {

    val ctx: Context = context

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val v = View.inflate(context, layout, null)

        val record = list[position]

        v.findViewById<TextView>(R.id.tvDate).run {
            text = record.date.toLong().toDate().format("dd MMM, yyyy")
        }

        v.findViewById<TextView>(R.id.tvAmount).run {
            text = if (record.amount > 0) record.amount.moneyFormat else ""
            visibility = if (record.amount > 0) View.VISIBLE else View.GONE
        }

        v.findViewById<TextView>(R.id.tvContent).run { text = record.content }

        v.findViewById<RelativeLayout>(R.id.container).run {
            setBackgroundColor(
                context.getColor(if (position % 2 == 0) android.R.color.white else R.color.paleGray)
            )
        }

        return v
    }

    data class Record(val date: String, val amount: Double, val content: String)
}

