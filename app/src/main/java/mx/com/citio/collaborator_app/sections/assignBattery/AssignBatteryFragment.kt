package mx.com.citio.collaborator_app.sections.stations


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_assign_battery.*
import mx.com.citio.FetchUserQuery
import mx.com.citio.collaborator_app.AssignMeBatteryActivity
import mx.com.citio.collaborator_app.R
import mx.com.citio.collaborator_app.ScanBatteryActivity
import mx.com.citio.collaborator_app.api.App
import mx.com.citio.collaborator_app.classes.onClick

class AssignBatteryFragment : Fragment() {

    private var selectedRfid: FetchUserQuery.Rfid? = null

    private var operatorId: String? = null


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return layoutInflater.inflate(R.layout.fragment_assign_battery, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        App.currentUser?.let { user ->
            Log.i("Operatpr", "operatorId ${user.operator?.id}")
            operatorId = user.operator?.id
            if(operatorId === null){
                rlAccessDenied.visibility = View.VISIBLE
                llOptions.visibility = View.GONE
            }
            user.rfids.firstOrNull().let {
                selectedRfid = it
            }
        }
        llBatteryBarcode.onClick {
            ScanBatteryActivity.start(context!!, this, operatorId!!)
            }

        llBatteryList.onClick {
            AssignMeBatteryActivity.start(context!!, this, operatorId!!)

        }
    }



    companion object {
        fun newInstance(): AssignBatteryFragment = AssignBatteryFragment()
    }


}
