package mx.com.citio.collaborator_app

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import kotlinx.android.synthetic.main.activity_tutorial.*
import mx.com.citio.collaborator_app.R
import mx.com.citio.collaborator_app.adapters.PagerAdapter
import mx.com.citio.collaborator_app.tutorial.TutorialFragment

class TutorialActivity : AppCompatActivity(), ViewPager.OnPageChangeListener {

    lateinit var adapter: PagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tutorial)

        adapter = PagerAdapter(supportFragmentManager).apply {
            addFragment(
                TutorialFragment.newInstance(
                    R.drawable.wizard_1,
                    "Administrar tus métodos de pago así como tu plan de servicio seleccionado",
                    "-"
                ), "t1"
            )
            addFragment(
                TutorialFragment.newInstance(
                    R.drawable.wizard_2,
                    "Consultar nuestras estaciones disponibles por cercanía o por zona Y EL ESTATUS DE baterías disponibles EN CADA UNA.",
                    "-"
                ), "t2"
            )
            addFragment(
                TutorialFragment.newInstance(
                    R.drawable.wizard_3,
                    "Consultar el estatus de tu cuenta y tus cambios disponibles, así como el historial de cambios y pagos realizados.",
                    "-"
                ), "t3"
            )
            addFragment(
                TutorialFragment.newInstance(
                    R.drawable.wizard_4,
                    "Solicitar cambios de batería adicionales.",
                    "-"
                ), "t4"
            )
            addFragment(
                TutorialFragment.newInstance(
                    R.drawable.wizard_5,
                    "Crear un ticket de soporte en caso de que requieras ayuda con algo relacionado a nuestro servicio.",
                    "-"
                ), "t5"
            )
        }

        viewPager.adapter = adapter
        viewPager.addOnPageChangeListener(this)
        pagerIndicator.setWithViewPager(viewPager)

        btnNext.setOnClickListener {
            if (viewPager.currentItem == adapter.count - 1)
                finish()
            else
                viewPager.setCurrentItem(viewPager.currentItem + 1, true)
        }
    }

    override fun onPageScrolled(i: Int, v: Float, i1: Int) {}

    override fun onPageSelected(i: Int) {
        btnNext.text =
            if (i == adapter.count - 1) getString(R.string.tutorial_continue) else getString(R.string.start)
    }

    override fun onPageScrollStateChanged(i: Int) {}
}
