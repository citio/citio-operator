package mx.com.citio.collaborator_app.adapters

import android.content.Context
import android.graphics.Color
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageButton
import android.widget.TextView
import mx.com.citio.collaborator_app.R
import mx.com.citio.collaborator_app.api.CitioNotification
import mx.com.citio.collaborator_app.classes.format
import mx.com.citio.collaborator_app.classes.onClick
import mx.com.citio.collaborator_app.classes.toDate


class NotificationAdapter(context: Context, val layout: Int, val list: ArrayList<CitioNotification>, val onReadNotification: (notification: CitioNotification) -> Unit) :
    ArrayAdapter<CitioNotification>(context, layout, list) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val v = View.inflate(context, layout, null)

        val notification = list[position]

        val tvDate = v.findViewById<TextView>(R.id.tvDate)
        val tvTitleNotification = v.findViewById<TextView>(R.id.tvTitleNotification)
        val tvMessage = v.findViewById<TextView>(R.id.tvMessage)
        val ibDelete = v.findViewById<ImageButton>(R.id.ibDelete)

        tvMessage.text = if(notification.plainText !== null && notification.plainText !== "") {if(notification.plainText.contains("&#x2F;")){notification.plainText.replace(
            "&#x2F;",
            "/"
        )}else{
            notification.plainText
        }}else{
            if(notification.body.contains("&#x2F;")){
                notification.body.replace(
                    "&#x2F;",
                    "/"
                )
            }else{
                notification.body
            }
        }
        tvDate.text = notification.createdAt.toLong().toDate().format("d / MMMM / yyyy")
        v.setBackgroundColor(
            if (notification.timesRead == 0) Color.parseColor("#E5ECDD") else Color.parseColor("#FBFBFB")
        )

        ibDelete.onClick { onReadNotification(notification) }

        return v
    }
}