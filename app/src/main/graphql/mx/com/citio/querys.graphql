query fetchUser {
  me {
    id
    name
    lastname
    email
    birthday
    confirm
    roles {
      app
      name
    }
    address {
      id
      streetName
      suburb
    }
    favoriteStations {
      id
      shortName
      latitude
      longitude
    }
    rfids {
      id
      serialNumber
      initDate
      endDate
      orders {
        id
        type
        createdAt
        station {
          id
        }
      }
      inscription {
        contract {
          plan {
            id
            name
            priceExtraExchange
            displayName
            repeatUnit
          }
        }
      }
      status {
        id
        folio
        barcode
        exchanges
        priceExtraExchange
        exchangesTotalPlan

        startDate
        endDate
        status
      }
    }
    inscriptions {
      id
      contract {
        id
        limitDate
        plan {
          id
          name
          exchanges
          price
          currency
          repeatUnit
          priceExtraExchange
          color
        }
      }
    }
    operator {
      id
    }
  }
}

query getNotifications($params: ParamsInput) {
  myNotifications(params: $params) {
    params {
      page
    }
    data {
      id
      type
      status
      createdAt
      timesRead
      plainText
      body
    }
  }
}

query getAlarms($params: ParamsInput) {
  alarms(params: $params) {
    params {
      page
      limit
      total
      totalPages
    }
    data {
      id
      status
      description
      createdAt
    }
  }
}

query getOrders($rfid: String!, $params: ParamsInput) {
  myOrders(rfid: $rfid, params: $params) {
    params {
      page
      limit
      total
      totalPages
    }
    data {
      id
      rfid {
        serialNumber
      }
      type
      createdAt
      batteryIn
      batteryOut
      station {
        id
        shortName
      }
    }
  }
}

query getPayments($rfid: ID!, $params: ParamsInput) {
  myPayments(rfidId: $rfid, params: $params) {
    params {
      page
      limit
      total
      totalPages
    }
    data {
      id
      externalId
      createdAt
      status
      processor
      card
      amount
    }
  }
}

query getPaymentMethods($params: ParamsInput) {
  myPaymentMethods(params: $params) {
    params {
      page
    }
    data {
      id
      type
      cardNumber
      brand
      createdAt
      default
      holderName
      processor
    }
  }
}

query getDefaultPaymentMethod {
  paymentFavorite {
    processor
    paymentMethodId
  }
}

query getStations($params: ParamsInput) {
  stations(params: $params) {
    params {
      page
      limit
      total
    }
    data {
      id
      live
      shortName
      latitude
      longitude
      zone
      openingHours
      closingTime
      address {
        id
        streetName
        exterior
        suburb
        zipcode
      }
      batteries {
        charged
        discharge
        color
      }
      comingSoon
    }
  }
}

query getPlans(
  $params: ParamsInput
  $filters: String
  $special: Boolean = false
  $type: RepeatUnitTypes
) {
  plans(params: $params, filters: $filters, special: $special, type: $type) {
    params {
      page
      limit
      total
    }
    data {
      id
      name
      displayName
      price
      currency
      repeatUnit
      kms
      exchanges
      priceExtraExchange
      color
      show
      unlimited
    }
  }
}

query getTerms {
  currentTerm {
    id
    term
    version
    current
  }
}

query getFaqs($params: ParamsInput) {
  faqs(params: $params) {
    params {
      page
      limit
      total
    }
    data {
      id
      answer
      question
    }
  }
}

query getBraintreeToken {
  braintreeClientToken
}

query getRfids($params: ParamsInput) {
  rfids(params: $params) {
    params {
      page
      limit
      total
    }
    data {
      id
      serialNumber
    }
  }
}

query getrfidAutocomplete($search: String!) {
  rfidAutocomplete(search: $search) {
    id
    serialNumber
    status {
      id
    }
  }
}

query batteryAutocomplete($search: String!) {
  batteryAutocomplete(search: $search) {
    id
    idHardware
    barcode
  }
}
